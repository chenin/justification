import pandas as pd

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.views.generic.edit import CreateView
from django.views.generic.detail import DetailView
from django.views.generic import TemplateView
from django.contrib.auth.models import User
from django.shortcuts import redirect
from django.contrib.auth import authenticate, login


from german.forms import ContestValueForm, ContestRelativeForm
from german.models import *
from adult.models import *
from mladvocate.models import *
from mladvocate.forms import *
from mladvocate.utils import *
from generate_justifications.dataset.load_data_and_model import load_model



class Thanks(TemplateView):
    form_class = FinalForm
    template_name = 'thanks.html'

    def get_context_data(self, request, *args, **kwargs):
        context = {}
        context['total_comment'] = self.request.user.commentfinal_set.count()
        return context

    def get(self, request, *args, **kwargs):
        total_comment = self.request.user.commentfinal_set.count()
        form = self.form_class()
        return render(request, self.template_name, {'form': form, 'total_comment': total_comment})

    def post(self, request, *args, **kwargs):
        context = self.get_context_data(self, request, *args, **kwargs)
        form = self.form_class(request.POST)
        if form.is_valid():
            user = self.request.user
            if form.cleaned_data['mail']:
                user.email = form.cleaned_data['mail']
                user.save()
            if form.cleaned_data['comment']:
                f_comm = CommentFinal(user=user, comment=form.cleaned_data['comment'])
                f_comm.save()
            context['form'] = self.form_class(request.POST)
            return render(request, 'thanks.html', context)

        context['form'] = self.form_class()
        return render(request, self.template_name, context)


class WelcomeView(TemplateView):
    template_name = "home.html"


class PresentationView(TemplateView):
    template_name = "presentation.html"

    def post(self, request, *args, **kwargs):
        if "start" in request.POST:
            if not request.user.is_authenticated:
                uname = str(len(User.objects.all()))
                pword = 'mdp_par_defaut'
                user = User.objects.create_user(uname, 'no@mail.com', pword)
                user.save()
                user = authenticate(request, username=uname, password=pword)
                if user is not None:
                    login(request, user)
            return redirect('mladvocate:task')
        else:
            return render(request, 'presentation.html')


class TaskExecution(TemplateView):

    template_name = "task.html"
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['case_form'] = CaseForm()

        if self.request.session.get('ongoing_task_id', False):
            task = Task.objects.get(pk=self.request.session['ongoing_task_id'])
        else:
            task = new_random_task(self.request.user)
            self.request.session['ongoing_task_id'] = task.pk
        samples = [text_from_sample(task.sample1), text_from_sample(task.sample2), text_from_sample(task.sample3)]
        context['help_type'] = task.help_type
        forms = [ContestValueForm(file=task.sample1), ContestValueForm(file=task.sample2), ContestValueForm(file=task.sample3)]
        files = [task.sample1, task.sample2, task.sample3]
        context['smpl_form_file'] = list(zip(samples, forms, files))
        file_typical_low = GermanFile(**TYPICAL_LOW_RISK)
        file_typical_low.save()
        file_typical_high = GermanFile(**TYPICAL_HIGH_RISK)
        file_typical_high.save()
        context['typical_low'] = file_typical_low
        context['typical_high'] = file_typical_high
        context['display_typical_low'] = text_from_sample(file_typical_low)
        context['display_typical_high'] = text_from_sample(file_typical_high)
        return context

    def get(self, request, *args, **kwargs):
        if self.request.user.task_set.exclude(user_answer__isnull=True).count() > 9:
            return redirect("mladvocate:thanks")
        context = self.get_context_data(**kwargs)
        return render(request, 'task.html', context)

    def post(self, request, *args, **kwargs):
        if self.request.user.task_set.exclude(user_answer__isnull=True).count() > 9:
            return redirect("mladvocate:thanks")
        context = self.get_context_data(**kwargs)
        id = self.request.session['ongoing_task_id']
        task = Task.objects.get(pk=id)
        if "case_answer" in request.POST:
            form = CaseForm(request.POST)
            if form.is_valid():
                task.user_answer = int(form.cleaned_data['case'])
                task.likert_confidence = int(form.cleaned_data['likert'])
                task.comment = str(form.cleaned_data['comment'])
                task.save()
                del self.request.session['ongoing_task_id']
            return redirect('/mla/task/', context)
        if "help_1" in request.POST:
            file, case_id, context['anchor_blocked'] = task.sample1, 1, "case_" + str(1)
        if "help_2" in request.POST:
            file, case_id, context['anchor_blocked'] = task.sample2, 2, "case_" + str(2)
        if "help_3" in request.POST:
            file, case_id, context['anchor_blocked'] = task.sample3, 3, "case_" + str(3)
        if context['help_type'] == 'MLA_VALUES':
            form = ContestValueForm(request.POST, file=file)
            if form.is_valid():
                contest_value = form.save(commit=False)
                contest_value.file = file
                contest_value.target_output = 1 - file.__dict__['y_pred']
                contest_value.save()
                mla_val_justif = display_mla_values(contest_value)
                context['smpl_form_file'][case_id - 1] = (*context['smpl_form_file'][case_id - 1], mla_val_justif)
            else:
                sample = context['smpl_form_file'][case_id - 1][0]
                file = context['smpl_form_file'][case_id - 1][2]
                context['smpl_form_file'][case_id - 1] = (sample, form, file )
            return render(request, 'task.html', context)
        if context['help_type'] == 'MLA_RELATIVE':
            plaintiff_file = file
            if bool(file.__dict__['y_pred']):
                relative_file = context['typical_high']
            else:
                relative_file = context['typical_low']
            form = ContestRelativeForm(request.POST, plaintiff_file=plaintiff_file, relative_file=relative_file)
            if form.is_valid():
                contest_relative = form.save(commit=False)
                contest_relative.plaintiff_file = plaintiff_file
                contest_relative.relative_file = relative_file
                contest_relative.target_plaintiff = 1 - file.__dict__['y_pred']
                contest_relative.output_relative = contest_relative.relative_file.__dict__['y_pred']
                contest_relative.save()
                mla_rel_justif = display_mla_relative(contest_relative)
                context['smpl_form_file'][case_id - 1] = (*context['smpl_form_file'][case_id - 1], mla_rel_justif)
            else:
                sample = context['smpl_form_file'][case_id - 1][0]
                file = context['smpl_form_file'][case_id - 1][2]
                context['smpl_form_file'][case_id - 1] = (sample, form, file )
            return render(request, 'task.html', context)
        if context['help_type'] == 'K_NEIGHBORS':
            text = kneighbors_justification(file)
            context['smpl_form_file'][case_id - 1] = (*context['smpl_form_file'][case_id - 1], text)
        if context['help_type'] == 'COUNTERFACT':
            text = cf_justification(file)
            context['smpl_form_file'][case_id - 1] = (*context['smpl_form_file'][case_id - 1], text)
        return render(request, 'task.html', context)


class ContestValueGeneric(DetailView):
    template_name = "contestvalue_detail.html"
    def get_context_data(self, **kwargs):
        ContestModel = self.model
        context = super().get_context_data(**kwargs)
        pk = self.kwargs['pk']
        # highlight the constraints put on dataset
        instance = ContestModel.objects.get(pk=pk)
        contest_data = {k: v for k, v in instance.__dict__.items()
                        if ('_value' in k or 'operator' in k) and v is not None}
        feature_set = {key.replace('_value', '').replace('_operator', '')
                       for key in contest_data.keys()}
        context['contest_feat_list'] = list(feature_set)
        context['target_output'] = label_output(instance.target_output, ContestModel)
        context['target_output_neg'] = label_output_neg(instance.target_output, ContestModel)

        # Original file and counterexample
        try:
            ce_obj = instance.counter_example.get()
            counter_ex_file = ce_obj.file.__dict__
            if counter_ex_file['y_pred']==1 and counter_ex_file['y_pred']==1:
                counter_ex_file['output'] = label_output(1, ContestModel)
            elif counter_ex_file['y_pred'] == 0 and counter_ex_file['y_pred'] == 0:
                counter_ex_file['output'] = label_output(0, ContestModel)
            else:
                counter_ex_file['output'] = 'MISMATCH PRED TRUE ERR'

            del counter_ex_file['y_pred'], counter_ex_file['y_true']
            original_file = instance.file.__dict__
            original_file['output'] = label_output(original_file['y_pred'], ContestModel)
            final_dict = {}
            for k, v in counter_ex_file.items():
                final_dict[k] = (v, original_file.get(k, None), str(contest_data.get(k + '_operator')) + '  ' + str(contest_data.get(k + '_value')))
            context['ce_dict'] = final_dict
        except:
            context['ce_dict'] = None

        # Proportion of validity of the model
        try:
            value = instance.valid_proportion.get().proportion
            context['proportion'] = value
        except:
            context['proportion'] = None

        original_constraints = []
        for i, feat in enumerate(feature_set):
            if (
    contest_data.get(feat + '_operator', None) is not None and contest_data.get(feat + '_value', None) is not None
            ):
                rule = (i, feat, contest_data[feat + '_operator'], contest_data[feat + '_value'], original_file[feat])
                original_constraints.append(rule)
        context['original_rules'] = original_constraints
        # Rule based model to enhance contestation
        rbm_obj = instance.rbm.filter(negative_bool=False).first()
        rules = rbm_obj.rules.all()
        rules_tuple = [r.__dict__ for r in rules]
        for predicate in rules_tuple:
            predicate['value'] = original_file[predicate['left']]
        if rules_tuple[0] != ('', '', ''):   # trivial model case
            context['extra_rules'] = rules_tuple
        context['precision'] = round(rbm_obj.__dict__['precision'] * 100, 2)
        context['100-precision'] = 100 - round(rbm_obj.__dict__['precision'] * 100, 2)
        context['size'] = int(rbm_obj.__dict__['size'])


        # Negative rule based model to contradict contestation
        try:
            rbm__obj_neg = instance.rbm.filter(negative_bool=True).first()
            rules = rbm__obj_neg.rules.all()
            rules_tuple = [r.__dict__ for r in rules]
            for predicate in rules_tuple:
                predicate['value'] = original_file[predicate['left']]
            if rules_tuple[0] != ('', '', ''):   # trivial model case
                context['extra_rules_neg'] = rules_tuple
            context['precision_neg'] = round(rbm__obj_neg.__dict__['precision'] * 100, 2)
            context['100-precision_neg'] = 100 - round(rbm__obj_neg.__dict__['precision'] * 100, 2)
            context['size_neg'] = int(rbm__obj_neg.__dict__['size'])
        except:
            context['extra_rules_neg'] = None
        return context


class ContestValueGerman(ContestValueGeneric):
    def __init__(self):
        self.model = GermanContestValue


class ContestValueAdult(ContestValueGeneric):
    def __init__(self):
        self.model = AdultContestValue


class ContestRelativeGeneric(DetailView):
    template_name = "contestrelative_detail.html"
    def get_context_data(self, **kwargs):

        ContestModel = self.model
        context = super().get_context_data(**kwargs)
        pk = self.kwargs['pk']
        # highlight the constraints put on dataset
        instance = self.model.objects.get(pk=pk)
        contest_data = {k: v for k, v in instance.__dict__.items()
                        if '_operator' in k and v is not None}
        feature_set = {key.replace('_operator', '')
                       for key in contest_data.keys()}
        context['contest_feat_list'] = list(feature_set)

        plaintiff_label = label_output(instance.target_plaintiff, ContestModel)
        context['target_plaintiff'] = label_output(instance.target_plaintiff, ContestModel)
        context['target_plaintiff_neg'] = label_output_neg(instance.target_plaintiff, ContestModel)
        context['output_relative'] = label_output(instance.output_relative, ContestModel)
        context['output_relative_neg'] = label_output_neg(instance.output_relative, ContestModel)
        # Original file and counterexample
        try:
            ce_obj = instance.counter_example.get()
            ce_plaintiff = ce_obj.plaintiff.__dict__
            # convert to label outputs for plaintiff
            if ce_plaintiff['y_pred']==1 and ce_plaintiff['y_pred']==1:
                ce_plaintiff['output'] = label_output(1, ContestModel)
            elif ce_plaintiff['y_pred'] == 0 and ce_plaintiff['y_pred'] == 0:
                ce_plaintiff['output'] = label_output(0, ContestModel)
            else:
                ce_plaintiff['output'] = 'MISMATCH PRED TRUE ERR'
            del ce_plaintiff['y_pred'], ce_plaintiff['y_true']

            # convert to label outputs for plaintiff
            ce_relative = ce_obj.relative.__dict__
            if ce_relative['y_pred']==1 and ce_relative['y_pred']==1:
                ce_relative['output'] = label_output(1, ContestModel)
            elif ce_relative['y_pred'] == 0 and ce_relative['y_pred'] == 0:
                ce_relative['output'] = label_output(0, ContestModel)
            else:
                ce_relative['output'] = 'MISMATCH PRED TRUE ERR'
            del ce_relative['y_pred'], ce_relative['y_true']

            original_file = instance.plaintiff_file.__dict__
            original_file['output'] = label_output(original_file['y_pred'], ContestModel)

            final_dict = {}
            for feat_name, plaintiff_val in ce_relative.items():
                final_dict[feat_name] = (
                    original_file.get(feat_name, None),
                    ce_plaintiff.get(feat_name, None),
                    str(contest_data.get(feat_name + '_operator')),
                    ce_relative.get(feat_name, None),
                    )
            context['ce_dict'] = final_dict
        except:
            context['ce_dict'] = None

        # Proportion of validity of the model
        try:
            value = instance.valid_proportion.get().proportion
            context['proportion'] = value
        except:
            context['proportion'] =None


        original_constraints = []
        for i, feat in enumerate(feature_set):
            rule = (i, "Your " + feat, contest_data[feat + '_operator'], "  the other's " + feat, original_file[feat])
            original_constraints.append(rule)
        context['original_rules'] = original_constraints

        # Rule based model to enhance contestation
        try:
            rbm_obj = instance.rbm.filter(negative_bool=False).first()
            rules = rbm_obj.rules.all()
            rules_tuple = [r.__dict__ for r in rules]
            for predicate in rules_tuple:
                predicate['value'] = original_file[predicate['left'][:-2]]
            if rules_tuple[0] != ('', '', ''):  # trivial model case
                context['extra_rules'] = rules_tuple
            context['precision'] = round(rbm_obj.__dict__['precision'] * 100, 2)
            context['100-precision'] = 100 - round(rbm_obj.__dict__['precision'] * 100, 2)
            context['size'] = int(rbm_obj.__dict__['size'])
        except:
            context['extra_rules'] = None

        # Negative rule based model to contradict contestation
        try:
            rbm__obj_neg = instance.rbm.filter(negative_bool=True).first()
            rules = rbm__obj_neg.rules.all()
            rules_tuple = [r.__dict__ for r in rules]
            for predicate in rules_tuple:
                predicate['value'] = original_file[predicate['left'][:-2]]
            if rules_tuple[0] != ('', '', ''):   # trivial model case
                context['extra_rules_neg'] = rules_tuple
            context['precision_neg'] = round(rbm__obj_neg.__dict__['precision'] * 100, 2)
            context['100-precision_neg'] = 100 - round(rbm__obj_neg.__dict__['precision'] * 100, 2)
            context['size_neg'] = int(rbm__obj_neg.__dict__['size'])
        except:
            context['extra_rules_neg'] = None
        return context


class ContestRelativeGerman(ContestRelativeGeneric):
    def __init__(self):
        self.model = GermanContestRelative


class ContestRelativeAdult(ContestRelativeGeneric):
    def __init__(self):
        self.model = AdultContestRelative
