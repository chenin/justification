from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import include, url

from mladvocate.views import *

urlpatterns = [
    path('task/', TaskExecution.as_view(), name='task'),
    path('thanks', Thanks.as_view(), name='thanks'),
]
