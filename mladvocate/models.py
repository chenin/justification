from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericRelation
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator, MinValueValidator

# Create your models here.


class CommentFinal(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    comment = models.CharField(max_length=3000, blank=True, null=True)


class CounterExampleValue(models.Model):
    contest_content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    contest_object_id = models.PositiveIntegerField()
    contest = GenericForeignKey("contest_content_type", "contest_object_id")

    file_content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, related_name='file_counter_example_value')
    file_object_id = models.PositiveIntegerField()
    file = GenericForeignKey("file_content_type", "file_object_id")


class CounterExampleRelative(models.Model):
    contest_content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    contest_object_id = models.PositiveIntegerField()
    contest = GenericForeignKey('contest_content_type', 'contest_object_id')

    plaintiff_content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, related_name="plaintiff_counter_example_relative")
    plaintiff_object_id = models.PositiveIntegerField()
    plaintiff = GenericForeignKey('plaintiff_content_type', 'plaintiff_object_id')

    relative_content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, related_name="relative_counter_example_relative")
    relative_object_id = models.PositiveIntegerField()
    relative = GenericForeignKey('relative_content_type', 'relative_object_id')


class ValidProportion(models.Model):
    size_all = models.PositiveIntegerField()
    size_valid = models.PositiveIntegerField()

    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object_contest = GenericForeignKey()


class Rule(models.Model):
    left = models.CharField(max_length=200)
    operator = models.CharField(max_length=100)
    right = models.CharField(max_length=200)

    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey()


class RuleBasedModel(models.Model):
    negative_bool = models.BooleanField()
    precision = models.FloatField()
    rules = GenericRelation(Rule)
    size = models.PositiveIntegerField(default=0)

    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object_contest = GenericForeignKey()

