from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, ButtonHolder, Submit
from german.models import *


CASE_CHOICE = (
    (1, "Cas 1"),
    (2, "Cas 2"),
    (3, "Cas 3"),
)

LIKERT_CHOICES = (
    (1, 1),
    (2, 2),
    (3, 3),
    (4, 4),
    (5, 5),
)


class CaseForm(forms.Form):
    case = forms.ChoiceField(choices=CASE_CHOICE, label="Cas à contester", initial='', widget=forms.Select(), required=True)
    likert = forms.ChoiceField(widget=forms.RadioSelect, choices=LIKERT_CHOICES)
    comment = forms.CharField(label="Commentaire", required=False, widget=forms.Textarea(attrs={
        'style': 'height: 10em; width: 30em;', 'placeholder': 'Commentaire libre (optionel)'}))

class FinalForm(forms.Form):
    mail = forms.CharField(required=False, widget=forms.TextInput(attrs={'placeholder': 'Entrez votre email (optionel)'}))
    comment = forms.CharField(label="Commentaire", required=False, widget=forms.Textarea(attrs={
        'style': 'height: 10em; width: 30em;', 'placeholder': 'Commentaire libre (optionel)'}))
