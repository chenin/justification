import pandas as pd
import operator
import random

from german.models import *
from adult.models import *
from generate_justifications.justify.justification import find_counter_example, compute_proportion_valid, sort_plaintiff_relative_cols
from generate_justifications.contest.contestation import ContestRel
from generate_justifications.dataset.load_data_and_model import load_data_and_pred
from generate_justifications.contest.contestation import ContestRel, ContestVal
from generate_justifications.dataset.german_simple import *
from generate_justifications.justify.counterfactuals import *

FULL_DATABASE = load_data_and_pred('german_simple')
DATABASE = FULL_DATABASE[FULL_DATABASE['y_true'] == FULL_DATABASE['y_pred']]


def cf_justification(sample):
    sample_dict = sample.__dict__
    dict_pre_pandas = dict()
    for col in sample_dict:
        dict_pre_pandas[col] = [sample_dict[col]]
    df_sample = pd.DataFrame.from_dict(dict_pre_pandas)[FEATURES_LIST + ['y_pred']]
    model = german_load_model()
    cf = find_cf(FULL_DATABASE, model, df_sample)
    cf_diff = {}
    for col in FEATURES_LIST:
        if cf[col].values[0]!=df_sample[col].values[0]:
            cf_diff[col] = {'old_val': df_sample[col].values[0], 'new_val': cf[col].values[0]}
    cf_text = " et ".join([key + " de " + str(val['old_val']) + " vers " + str(val['new_val']) for key, val in cf_diff.items()])
    class_pred = CLASS_NAMES[sample_dict['y_pred']]
    alt_class = CLASS_NAMES[1 - sample_dict['y_pred']]
    text = """Le modèle a classé ce dossier à {class_pred} risque. 
    <br>
    En modifiant {cf_text}, le modèle classerait le dossier comme {alt_class} risque. 
    """.format(cf_text=cf_text, class_pred=class_pred, alt_class=alt_class)
    return text


def kneighbors_justification(sample):
    sample_dict = sample.__dict__
    dict_pre_pandas = dict()
    for col in sample_dict:
        dict_pre_pandas[col] = [sample_dict[col]]
    df_sample = pd.DataFrame.from_dict(dict_pre_pandas)
    neightbors_indices = get_k_neighbors(df_sample)
    neighbors = FULL_DATABASE.loc[neightbors_indices]
    class_pred = CLASS_NAMES[sample_dict['y_pred']]
    k = len(neighbors)
    proportion = str(round((1 - neighbors['y_true'].mean()) * 100, 1))
    text = """Le modèle a classé ce dossier à {class_pred} risque. 
    <br>
    Pour prendre cette décision, le modèle est censé s'appuyer sur {k} dossiers similaires classés par des agents et qui font référence. 
    Il suffit qu'un tiers de ces dossiers soit classé à haut risque pour que les dossiers similaires le soient également.
    <br>
    Ici, {proportion} % des dossiers sont classés à haut risque. 
    """.format(class_pred=class_pred, k=k, proportion=proportion)
    return text


def contest_obj_to_rules_rel(contest_val_obj):

    contest_data = {k: v for k, v in contest_val_obj.__dict__.items()
                    if '_operator' in k and v is not None}
    feature_set = {key.replace('_operator', '')
                   for key in contest_data.keys()}

    original_constraints = []
    for i, feat in enumerate(feature_set):
        rule = (feat + " du plaignant ", contest_data[feat + '_operator'], feat + " de la référence ")
        original_constraints.append(rule)
    return original_constraints


def display_mla_relative(contest_val_obj):
    contest_rules_all = " et que ".join([" ".join([str(i) for i in r]) for r in contest_obj_to_rules_rel(contest_val_obj)])
    target_plaintiff = CLASS_NAMES[contest_val_obj.target_plaintiff]
    relative_class = CLASS_NAMES[contest_val_obj.output_relative]
    val_prop_obj = contest_val_obj.valid_proportion.get()
    size_all, size_valid = val_prop_obj.size_all, val_prop_obj.size_valid
    percentage_valid = round(float(size_valid) / size_all * 100, 1)
    text = """
    Vous remettez en question cette décision en la comparant au dossier de référence. 
    Compte tenu du fait que le dossier de référence est classé comme <b>{relative_class}</b> risque et que <b>{contest_rules_all}</b>, 
    vous considérez que le dossier du plaignant devrait aussi être classé comme <b>{target_plaintiff}</b> risque.<br>
    Dans les données historiques, utilisées par le modèle, il y a <b>{size_all}</b> situations similaires 
    dans lesquelles, <b>{size_valid}</b> plaignants sont classés comme <b>{target_plaintiff}</b> risques (<b>{percentage_valid}</b> %). <br>
    Le système de justification tente d'affiner ces critères de deux manières : dans le sens de votre contestation, 
    ou dans le sens du modèle (à l'encontre de votre contestation). <br>"""
    text = text.format(contest_rules_all=contest_rules_all, target_plaintiff=target_plaintiff, relative_class=relative_class,
                       size_all=size_all, size_valid=size_valid, percentage_valid=percentage_valid)
    if len(contest_val_obj.rbm.filter(negative_bool=True)) > 0:
        rbm__obj_neg = contest_val_obj.rbm.filter(negative_bool=True).first()
        rules = rbm__obj_neg.rules.all()
        rules_tuple = [r.__dict__ for r in rules]
        rule_model_size = rbm__obj_neg.size
        rule_model_rule_text = " and ".join([r['left'] + r['operator'] + r['right'] for r in rules_tuple])
        rule_model_percentage = round(rbm__obj_neg.precision * 100, 1)
        text += """
        <u>Dans le sens du modèle :</u> si seules les <b>{rule_model_size}</b> situations qui satisfont également 
        <b>{rule_model_rule_text}</b> (ce qui est le cas ici) sont prises en compte, 
        alors seulement <b>{rule_model_percentage}</b> % des plaignants sont classés comme <b>{target_plaintiff}</b> risque. <br>"""
        text = text.format(rule_model_size=rule_model_size,
                           rule_model_rule_text=rule_model_rule_text,
                           rule_model_percentage=rule_model_percentage,
                           target_plaintiff=target_plaintiff)
    else:
        text += "<u>Dans le sens du modèle :</u> aucun contre-argument significatif. <br>"
    if len(contest_val_obj.rbm.filter(negative_bool=False)) > 0:
        rbm__obj_pos = contest_val_obj.rbm.filter(negative_bool=False).first()
        rules = rbm__obj_pos.rules.all()
        rules_tuple = [r.__dict__ for r in rules]
        rule_challenger_size = rbm__obj_pos.size
        rule_challenger_rule_text = " et ".join([r['left'] + r['operator'] + r['right'] for r in rules_tuple])
        rule_challenger_percentage = round(rbm__obj_pos.precision * 100, 1)
        text += """
        <u>Dans le sens de votre contestation :</u> si seuls les <b>{rule_challenger_size}</b> situations qui satisfont
        également <b>{rule_challenger_rule_text}</b> (ce qui est le cas ici) sont prises en compte, 
        alors <b>{rule_challenger_percentage}</b> % des plaignants sont classés comme <b>{target_plaintiff}</b> risque."""
        text = text.format(rule_challenger_size=rule_challenger_size,
                           rule_challenger_rule_text=rule_challenger_rule_text,
                           rule_challenger_percentage=rule_challenger_percentage,
                           target_plaintiff=target_plaintiff)
    else:
        text += "<u>Dans le sens de votre contestation :</u> aucun contre-argument significatif."
    return text


def contest_obj_to_rules(contest_val_obj):

    contest_data = {k: v for k, v in contest_val_obj.__dict__.items()
                    if ('_value' in k or 'operator' in k) and v is not None}
    feature_set = {key.replace('_value', '').replace('_operator', '')
                   for key in contest_data.keys()}
    original_constraints = []
    for i, feat in enumerate(feature_set):
        if (contest_data.get(feat + '_operator', None) is not None and contest_data.get(feat + '_value', None) is not None):
            rule = (feat, contest_data[feat + '_operator'], contest_data[feat + '_value'])
            original_constraints.append(rule)
    return original_constraints

def display_mla_values(contest_val_obj):
    contest_rules_all = " et ".join([" ".join([str(i) for i in r]) for r in contest_obj_to_rules(contest_val_obj)])
    expected_class = CLASS_NAMES[contest_val_obj.target_output]
    val_prop_obj = contest_val_obj.valid_proportion.get()
    size_all, size_valid = val_prop_obj.size_all, val_prop_obj.size_valid
    percentage_valid = round(float(size_valid) / size_all * 100, 1)
    text = """Vous remettez en question cette décision car vous pensez que la condition <b>{contest_rules_all}</b> devrait 
    conduire le modèle à classer ce dossier comme <b>{expected_class}</b> risque. <br>
    Dans les données historiques utilisées par le modèle, <b>{size_all}</b> dossiers satisfont cette condition. 
    Parmi eux, <b>{size_valid}</b> sont classés comme <b>{expected_class}</b> risque (<b>{percentage_valid}</b> %). <br>
    Le système de justification tente d'affiner ces critères de deux manières : dans le sens de votre contestation, 
    ou dans le sens du modèle (à l'encontre de votre contestation). <br>"""
    text = text.format(contest_rules_all=contest_rules_all, expected_class=expected_class,
                       size_all=size_all, size_valid=size_valid, percentage_valid=percentage_valid)
    if len(contest_val_obj.rbm.filter(negative_bool=True)) > 0:
        rbm__obj_neg = contest_val_obj.rbm.filter(negative_bool=True).first()
        rules = rbm__obj_neg.rules.all()
        rules_tuple = [r.__dict__ for r in rules]
        rule_model_size = rbm__obj_neg.size
        rule_model_rule_text = " et ".join([r['left'] + r['operator'] + r['right'] for r in rules_tuple])
        rule_model_percentage = round(rbm__obj_neg.precision * 100, 1)
        text += """
        <u>Dans le sens du modèle :</u> si seuls les <b>{rule_model_size}</b> dossiers qui satisfont également 
        <b>{rule_model_rule_text}</b> (ce qui est le cas ici) sont pris en compte, 
        alors seulement <b>{rule_model_percentage}</b> % sont classés comme <b>{expected_class}</b> risque. <br>"""
        text = text.format(rule_model_size=rule_model_size,
                           rule_model_rule_text=rule_model_rule_text,
                           rule_model_percentage=rule_model_percentage,
                           expected_class=expected_class)
    else:
        text += "<u>Dans le sens du modèle :</u> aucun contre-argument significatif. <br>"
    if len(contest_val_obj.rbm.filter(negative_bool=False)) > 0:
        rbm__obj_pos = contest_val_obj.rbm.filter(negative_bool=False).first()
        rules = rbm__obj_pos.rules.all()
        rules_tuple = [r.__dict__ for r in rules]
        rule_challenger_size = rbm__obj_pos.size
        rule_challenger_rule_text = " et ".join([r['left'] + r['operator'] + r['right'] for r in rules_tuple])
        rule_challenger_percentage = round(rbm__obj_pos.precision * 100, 1)
        text += """
        <u>Dans le sens de votre contestation :</u> si seuls les <b>{rule_challenger_size}</b> dossiers qui satisfont 
        également <b>{rule_challenger_rule_text}</b> (ce qui est le cas ici) sont pris en compte, 
        alors <b>{rule_challenger_percentage}</b> % sont classés comme <b>{expected_class}</b> risque."""
        text = text.format(rule_challenger_size=rule_challenger_size,
                           rule_challenger_rule_text=rule_challenger_rule_text,
                           rule_challenger_percentage=rule_challenger_percentage,
                           expected_class=expected_class)
    else:
        text += "<u>Dans le sens de votre contestation :</u> aucun contre-argument significatif."
    return text


def new_random_task(user):
    task_samples = []
    true_answer = random.choice([1, 2, 3])
    for idx, sample in enumerate(DATABASE.sample(3).iterrows()):
        db_obj = GermanFile(**sample[1].to_dict())
        if idx == true_answer - 1:
            db_obj.y_pred = 1 - db_obj.y_pred
            db_obj.y_true = 1 - db_obj.y_true
        db_obj.save()
        task_samples.append(db_obj)
    task_count_per_help = [user.task_set.filter(help_type=help[0]).count() for help in CHOICE_HELP]
    remaining_tasks_per_help = np.ones(len(CHOICE_HELP)) * 2 - np.array(task_count_per_help)
    help_type = random.choices(CHOICE_HELP, weights=remaining_tasks_per_help)[0][0]
    new_obj = Task(user=user, sample1=task_samples[0], sample2=task_samples[1], sample3=task_samples[2],
                   help_type=help_type, true_answer=true_answer)
    new_obj.save()
    return new_obj


def text_from_sample(sample_obj):
    class_pred = CLASS_NAMES[sample_obj.__dict__['y_pred']]
    text = """
    Le demandeur de crédit est un <b>{personal_status}</b> de <b>{age}</b> ans, employé comme <b>{job}</b> depuis <b>{employment}</b> années. <br>
    Il fait une demande pour un crédit de <b>{credit_amount}</b> Mark sur une durée de <b>{duration}</b> mois pour financer <b>{purpose}</b>. <br>
    Son historique de crédit indique <b>{credit_history}</b>, ses propriétés s'élèvent à <b>{property_magnitude}</b>.
    Il est logé <b>{housing}</b> et un <b>{other_parties}</b> est tierce partie de l'emprunt.
    Statut de travailleur étranger : <b>{foreign_worker}</b><br> 
    Cette demande est classée à <b>{class_pred}</b> risque par le modèle. 
    """.format(**sample_obj.__dict__, class_pred=class_pred)
    return text


def label_output_german(output_bool):
    if output_bool:
        return 'good'
    else:
        return 'bad'


def label_output_adult(output_bool):
    if output_bool:
        return '>50k'
    else:
        return '<=50k'


def label_output(target_bool, contest_obj):
    if contest_obj == AdultContestValue or contest_obj == AdultContestRelative:
        return label_output_adult(target_bool)
    elif contest_obj == GermanContestValue or contest_obj == GermanContestRelative:
        return label_output_german(target_bool)
    else:
        raise ValueError("ContestModel should be GermanContest or AdultContest")


def label_output_neg(target_bool, contest_obj):
    if contest_obj == AdultContestValue or contest_obj == AdultContestRelative:
        return label_output_adult(not target_bool)
    elif contest_obj == GermanContestValue or contest_obj == GermanContestRelative:
        return label_output_german(not target_bool)
    else:
        raise ValueError("ContestModel should be GermanContest or AdultContest")


def rule_symbol_to_operator(symbol):
    if symbol == ' is ':
        return operator.eq
    elif symbol == ' is not ':
        return operator.ne
    elif symbol == ' > ':
        return operator.gt
    elif symbol == ' <= ':
        return operator.le
    else:
        raise ValueError("Symbol " + symbol + " is not unvalid. ")


def instance_check_rules(rules, instance, neg):
    if neg:
        res = False
    else:
        res = True
    if str(rules)==str([('', '', '')]):
        return res
    for rule in rules:
        try:
            if neg:
                res += rule_symbol_to_operator(rule[1])(instance[rule[0]], float(rule[2]))
            else:
                res *= rule_symbol_to_operator(rule[1])(instance[rule[0]], float(rule[2]))
        except ValueError:
            if neg:
                res += rule_symbol_to_operator(rule[1])(instance[rule[0]], rule[2])
            else:
                res *= rule_symbol_to_operator(rule[1])(instance[rule[0]], rule[2])
    if neg:
        return not res
    else:
        return res


def convert_operator(symbol):
    if symbol == 'est':
        return operator.eq
    if symbol == "n'est pas":
        return operator.ne
    elif symbol == '>=':
        return operator.ge
    elif symbol == '<=':
        return operator.le
    else:
        raise ValueError("Symbol " + symbol + " is not valid. ")


def contestvalue_db_obj_to_internal_class(contest_db_obj, target_c):
    contest_data = {k: v for k, v in contest_db_obj.__dict__.items()
                    if ('_value' in k or 'operator' in k) and v is not None}
    predicate_list = []
    values = {}
    feature_set = {key.replace('_value', '').replace('_operator', '')
                   for key in contest_data.keys()}
    for feat in feature_set:
        if contest_data.get(feat + '_operator', None) is not None:
            op = convert_operator(contest_data[feat + '_operator'])
            predicate_list.append((feat, op, feat))
            values[feat] = contest_data[feat + '_value']
    return ContestVal(predicate_list, target_c, values)


def contestrelative_db_obj_to_internal_class(contest_db_obj, target_c, target_e):

    contest_data = {k: v for k, v in contest_db_obj.__dict__.items()
                    if '_operator' in k and v is not None}
    predicate_list = []
    feature_set = {key.replace('_operator', '') for key in contest_data.keys()}
    for feat in feature_set:
        if contest_data.get(feat + '_operator', None) is not None:
            op = convert_operator(contest_data[feat + '_operator'])
            predicate_list.append((feat, op, feat))
    return ContestRel(predicate_list, target_c, target_e)

#
# def find_and_save_rule_based_model(cv, data, instance, negative):
#     applicant_file = instance.file
#     rbms = rule_based_model(cv, data, negative=negative)
#     if rbms is not None:
#         # selecting a model NOT satisfied by the user
#         good_mod_found = False
#         for rbm in rbms:
#             if instance_check_rules(rbm[0], applicant_file.__dict__, neg=~negative):
#                 good_mod_found = True
#                 break
#         if not good_mod_found:
#             rbms = rule_based_model(cv, data, n_models=100, precision=.7)
#             if rbms is not None:
#                 for rbm in rbms:
#                     if instance_check_rules(rbm[0], applicant_file.__dict__, neg=~negative):
#                         good_mod_found = True
#                         break
#         if good_mod_found:
#             rbm_obj = RuleBasedModel(content_object_contest=instance,
#                                      precision=rbm[1],
#                                      negative_bool=negative)
#             rbm_obj.save()
#             for pred in rbm[0]:
#                 rule = Rule(left=pred[0], operator=pred[1], right=pred[2], content_object=rbm_obj)
#                 rule.save()
