from django.apps import AppConfig


class MladvocateConfig(AppConfig):
    name = 'mladvocate'


class ActivityAppConfig(AppConfig):
    name = 'mladvocate'

    def ready(self):
        import mladvocate.signals
