from django.contrib import admin

from mladvocate.models import *

admin.site.register(CounterExampleValue)
admin.site.register(CounterExampleRelative)
admin.site.register(ValidProportion)
admin.site.register(Rule)
admin.site.register(CommentFinal)
admin.site.register(RuleBasedModel)
