import operator
from django.db.models.signals import post_save

from german.models import *
from adult.models import *
from mladvocate.utils import *
from generate_justifications.justify.justification import find_counter_example, compute_proportion_valid, sort_plaintiff_relative_cols
from generate_justifications.justify.rule_based_justifications import *
from generate_justifications.contest.contestation import ContestRel
from generate_justifications.dataset.load_data_and_model import load_data_and_pred

# Loading the database and the model
DATA = load_data_and_pred("german_simple")
DATA = DATA[DATA['y_true'] == DATA['y_pred']]


def create_justifications_values(sender, instance, **kwargs):

    # Creating a Contestation object
    target_c = int(instance.__dict__['target_output'])

    applicant_file = instance.file
    cv = contestvalue_db_obj_to_internal_class(instance, target_c)
    # generating a counterexample

    # computing the proportion of validity of the contestation
    size_valid, size_all = compute_valid_size(cv, DATA)
    p_obj = ValidProportion(content_object_contest=instance, size_valid=size_valid, size_all=size_all)
    p_obj.save()

    # computing model more precise than the model of the user
    applicant_file = instance.file.__dict__
    dict_pre_pandas = {k: [applicant_file.get(k)] for k in DATA.columns}
    applicant_file_pandas = pd.DataFrame.from_dict(dict_pre_pandas)
    rbm, rbm_neg = pro_and_cons_rules(applicant_file_pandas, cv, DATA)
    if rbm:
        rbm_obj = RuleBasedModel(content_object_contest=instance,
                                 precision=rbm[2],
                                 size=rbm[1],
                                 negative_bool=False)
        rbm_obj.save()
        for pred in rbm[0]:
            rule = Rule(left=pred[0], operator=pred[1], right=pred[2], content_object=rbm_obj)
            rule.save()
    if rbm_neg:
        rbm_obj = RuleBasedModel(content_object_contest=instance,
                                 precision=rbm_neg[2],
                                 size=rbm_neg[1],
                                 negative_bool=True)
        rbm_obj.save()
        for pred in rbm_neg[0]:
            rule = Rule(left=pred[0], operator=pred[1], right=pred[2], content_object=rbm_obj)
            rule.save()

post_save.connect(create_justifications_values, sender=GermanContestValue)
post_save.connect(create_justifications_values, sender=AdultContestValue)


def create_justifications_relative(sender, instance, **kwargs):
    # Loading the database and the model
    # Creating a Contestation object
    target_c = instance.__dict__['target_plaintiff']
    target_e = instance.__dict__['output_relative']
    cr = contestrelative_db_obj_to_internal_class(instance, target_c, target_e)

    size_valid, size_all = compute_valid_size(cr, DATA)
    p_obj = ValidProportion(content_object_contest=instance, size_valid=size_valid, size_all=size_all)
    p_obj.save()

    # computing model more precise than the model of the user
    applicant_file = instance.plaintiff_file.__dict__
    dict_pre_pandas = {k: [applicant_file.get(k)] for k in DATA.columns}
    applicant_file_pandas = pd.DataFrame.from_dict(dict_pre_pandas)
    rbm, rbm_neg = pro_and_cons_rules(applicant_file_pandas, cr, DATA)
    if rbm:
        rbm_obj = RuleBasedModel(content_object_contest=instance,
                                 precision=rbm[2],
                                 size=rbm[1],
                                 negative_bool=False)
        rbm_obj.save()
        for pred in rbm[0]:
            rule = Rule(left=pred[0], operator=pred[1], right=pred[2], content_object=rbm_obj)
            rule.save()
    if rbm_neg:
        rbm_obj = RuleBasedModel(content_object_contest=instance,
                                 precision=rbm_neg[2],
                                 size=rbm_neg[1],
                                 negative_bool=True)
        rbm_obj.save()
        for pred in rbm_neg[0]:
            rule = Rule(left=pred[0], operator=pred[1], right=pred[2], content_object=rbm_obj)
            rule.save()


post_save.connect(create_justifications_relative, sender=AdultContestRelative)
post_save.connect(create_justifications_relative, sender=GermanContestRelative)
