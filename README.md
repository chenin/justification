L'organisation conseillé pour le dossier du code est la suivante : 

```justification
├── draft.ipynb
├── src
│   ├── db.sqlite3
│   ├── draft.ipynb
│   ├── generate_justifications
│   ├── german
│   ├── __init__.py
│   ├── justifyinter
│   ├── manage.py
│   ├── __pycache__
│   ├── random_forest_trained_model.joblib
│   ├── requirements.txt
│   ├── static
│   └── templates
└── venv
```


En pratique :

```
 mkdir justification
 cd justification
 git clone https://gitlab.inria.fr/chenin/justification
 mv justification src
```

Ensuite on crée l'environnement virutel :

```
 virtualenv -p python3 env
 source env/bin/activate
 cd src
 pip install -r requirements.txt
```

Vous devriez désormais pouvoir executer l'application en local avec la commande suivante : 

```python manage.py runserver```

ou bien: 

```python3 manage.py runserver``` 

(en fonction de la version de python par défaut)

Créer le fichier de config local (il est dans le gitignore pour ne pas le mettre en prod)

``` cp justifyinter/settings/base.py justifyinter/settings/local.py ```

Il faudra ensuite configurer dans le fichier ```justifyinter/settings/production.py```

```
DATABASES = { ... 

ALLOWED_HOSTS = [ ... 
```
  
et sans doute le fichier ```justifyinter/wsgi.py``` qui définit l'application. 