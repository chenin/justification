from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import include, url
from django.views.generic.base import TemplateView

from german.views import *
from mladvocate.views import *

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', WelcomeView.as_view(), name='welcome_page'),
    path('presentation/', PresentationView.as_view(), name='presentation'),
    url(r'^mla/', include(('mladvocate.urls', 'mladvocate'), namespace='mladvocate')),
    url(r'^german/', include(('german.urls', 'german'), namespace='german')),
    # url(r'^adult/', include(('adult.urls', 'adult'), namespace='adult')),
    path('value_justification/german/<int:pk>/', ContestValueGerman.as_view(), name='value_justification'),
    # path('value_justification/adult/<int:pk>/', ContestValueAdult.as_view(), name='value_justification'),
    path('relative_justification/german/<int:pk>/', ContestRelativeGerman.as_view(), name='relative_justification'),
    # path('relative_justification/adult/<int:pk>/', ContestRelativeAdult.as_view(), name='relative_justification'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
