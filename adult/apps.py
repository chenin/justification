from django.apps import AppConfig


class AdultConfig(AppConfig):
    name = 'adult'


class ActivityAppConfig(AppConfig):
    name = 'adult'

    def ready(self):
        import mladvocate.signals