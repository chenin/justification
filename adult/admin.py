from django.contrib import admin

from adult.models import *

admin.site.register(AdultFile)
admin.site.register(AdultContestValue)
admin.site.register(AdultContestRelative)
