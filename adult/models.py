from django.db import models
from django.contrib.contenttypes.fields import GenericRelation

from mladvocate.models import *


CHOICE_WORKCLASS = (
    ("Private", "Private"),  ("Self-emp-not-inc", "Self-emp-not-inc"),
    ("Self-emp-inc", "Self-emp-inc"),  ("Federal-gov", "Federal-gov"),
    ("Local-gov", "Local-gov"),  ("State-gov", "State-gov"),
    ("Without-pay", "Without-pay"),  ("Never-worked", "Never-worked"),
)
CHOICE_EDUCATION = (
    ("HS-grad", "HS-grad"), ("Some-college", "Some-college"),
    ("11th", "11th"), ("Bachelors", "Bachelors"),
    ("Prof-school", "Prof-school"), ("Assoc-acdm", "Assoc-acdm"),
    ("Assoc-voc", "Assoc-voc"), ("9th", "9th"), ("7th-8th", "7th-8th"),
    ("12th", "12th"), ("Masters", "Masters"), ("1st-4th", "1st-4th"),
    ("10th", "10th"), ("Doctorate", "Doctorate"), ("5th-6th", "5th-6th"),
    ("Preschool", "Preschool"),
)
CHOICE_MARITAL_STATUS = (
    ("not married", "not married"), ("married", "married"), ("divorced", "divorced"),
)
CHOICE_OCCUPATION = (
    ("Other-service", "Other-service"),
    ("Craft-repair", "Craft-repair"),("Tech-support", "Tech-support"),
    ("Sales", "Sales"), ("Exec-managerial", "Exec-managerial"),
    ("Prof-specialty", "Prof-specialty"),
    ("Handlers-cleaners", "Handlers-cleaners"),
    ("Machine-op-inspct", "Machine-op-inspct"),
    ("Adm-clerical", "Adm-clerical"),
    ("Farming-fishing", "Farming-fishing"),
    ("Transport-moving", "Transport-moving"),
    ("Priv-house-serv", "Priv-house-serv"),
    ("Protective-serv", "Protective-serv"),
    ("Armed-Forces", "Armed-Forces"),
)
CHOICE_RELATIONSHIP = (
    ("Not-in-family", "Not-in-family"), ("Other-relative", "Other-relative"),
    ("Husband", "Husband"), ("Wife", "Wife"), ("Own-child", "Own-child"),
    ("Unmarried", "Unmarried"),
)
CHOICE_RACE = (
    ("White", "White"),
    ("Asian-Pac-Islander", "Asian-Pac-Islander"),
    ("Amer-Indian-Eskimo", "Amer-Indian-Eskimo"),
    ("Other", "Other"),
    ("Black", "Black"),
)
CHOICE_SEX = (
("Male", "Male"),
("Female", "Female"),
)
CHOICE_NATIVE_COUNTRY = (
    ("United-States", "United-States"), ("Cambodia", "Cambodia"),
    ("England", "England"), ("Puerto-Rico", "Puerto-Rico"),
    ("Canada", "Canada"), ("Germany", "Germany"),
    ("Outlying-US(Guam-USVI-etc)", "Outlying-US(Guam-USVI-etc)"),
    ("India", "India"), ("Japan", "Japan"),
    ("Greece", "Greece"), ("South", "South"),
    ("China", "China"), ("Cuba", "Cuba"),
    ("Iran", "Iran"), ("Honduras", "Honduras"),
    ("Philippines", "Philippines"), ("Italy", "Italy"),
    ("Poland", "Poland"), ("Jamaica", "Jamaica"),
    ("Vietnam", "Vietnam"), ("Mexico", "Mexico"),
    ("Portugal", "Portugal"), ("Ireland", "Ireland"),
    ("France", "France"), ("Dominican-Republic", "Dominican-Republic"),
    ("Laos", "Laos"), ("Ecuador", "Ecuador"),
    ("Taiwan", "Taiwan"), ("Haiti", "Haiti"),
    ("Columbia", "Columbia"), ("Hungary", "Hungary"),
    ("Guatemala", "Guatemala"), ("Nicaragua", "Nicaragua"),
    ("Scotland", "Scotland"), ("Thailand", "Thailand"),
    ("Yugoslavia", "Yugoslavia"), ("El-Salvador", "El-Salvador"),
    ("Trinadad&Tobago", "Trinadad&Tobago"), ("Peru", "Peru"),
    ("Hong", "Hong"), ("Holand-Netherlands", "Holand-Netherlands"),
)
CATE_OPERATOR = (
    ('', ''),
    ('IS', 'IS'),
    ('IS NOT', 'IS NOT'),
)
INT_OPERATOR = (
    ('', ''),
    ('EQUALS', 'EQUALS'),
    ('DIFFERS', 'DIFFERS'),
    ('>', '>'),
    ('<', '<'),
    ('>=', '>='),
    ('<=', '<='),
)
TARGET_CHOICES = (
    ('1', '>50K'),
    ('0', '<=50K'),
)

class AdultFile(models.Model):
    age = models.IntegerField(default=36)
    sex = models.CharField(max_length=100, choices=CHOICE_SEX, default=CHOICE_SEX[0])
    race = models.CharField(max_length=100, choices=CHOICE_RACE, default=CHOICE_RACE[0])
    native_country = models.CharField(max_length=100, choices=CHOICE_NATIVE_COUNTRY, default=CHOICE_NATIVE_COUNTRY[0])
    marital_status = models.CharField(max_length=100, choices=CHOICE_MARITAL_STATUS, default=CHOICE_MARITAL_STATUS[0])
    relationship = models.CharField(max_length=100, choices=CHOICE_RELATIONSHIP, default=CHOICE_RELATIONSHIP[0])
    education = models.CharField(max_length=100, choices=CHOICE_EDUCATION, default=CHOICE_EDUCATION[0])
    education_num  = models.IntegerField(default=9)
    hours_per_week  = models.IntegerField(default=38)
    workclass = models.CharField(max_length=100, choices=CHOICE_WORKCLASS, default=CHOICE_WORKCLASS[0])
    occupation = models.CharField(max_length=100, choices=CHOICE_OCCUPATION, default=CHOICE_OCCUPATION[0])
    capital_gain  = models.IntegerField(default=700)
    capital_loss  = models.IntegerField(default=0)
    y_true = models.NullBooleanField(blank=True, null=True)
    y_pred = models.BooleanField(default=True)
    counter_example_value = GenericRelation(CounterExampleValue, related_name='file_counter_example_value', content_type_field='file_content_type', object_id_field='file_object_id')
    counter_example_plaintiff = GenericRelation(CounterExampleRelative, related_name='plaintiff_counter_example_relative', content_type_field='plaintiff_content_type', object_id_field='plaintiff_object_id')
    counter_example_relative = GenericRelation(CounterExampleRelative, related_name='relative_counter_example_relative', content_type_field='relative_content_type', object_id_field='relative_object_id')


class AdultContestValue(models.Model):
    file = models.ForeignKey(AdultFile, on_delete=models.CASCADE)
    age_operator = models.CharField(max_length=99, choices=INT_OPERATOR, verbose_name="Your age", blank=True, null=True)
    age_value = models.IntegerField(verbose_name='', blank=True, null=True)
    workclass_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name="Your workclass", blank=True, null=True)
    workclass_value = models.CharField(max_length=100, choices=CHOICE_WORKCLASS, verbose_name='', blank=True, null=True)
    education_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name="Your education", blank=True, null=True)
    education_value = models.CharField(max_length=100, choices=CHOICE_EDUCATION, verbose_name='', blank=True, null=True)
    education_num_operator = models.CharField(max_length=100, choices=INT_OPERATOR, verbose_name="Your education number", blank=True, null=True)
    education_num_value = models.IntegerField(verbose_name='', blank=True, null=True)
    marital_status_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name="Your marital status", blank=True, null=True)
    marital_status_value = models.CharField(max_length=100, choices=CHOICE_MARITAL_STATUS, verbose_name='', blank=True, null=True)
    occupation_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name="Your occupation", blank=True, null=True)
    occupation_value = models.CharField(max_length=100, choices=CHOICE_OCCUPATION, verbose_name='', blank=True, null=True)
    relationship_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name="Your relationship status", blank=True, null=True)
    relationship_value = models.CharField(max_length=100, choices=CHOICE_RELATIONSHIP, verbose_name='', blank=True, null=True)
    race_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name="Your race", blank=True, null=True)
    race_value = models.CharField(max_length=100, choices=CHOICE_RACE, verbose_name='', blank=True, null=True)
    sex_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name="Your sex", blank=True, null=True)
    sex_value = models.CharField(max_length=100, choices=CHOICE_SEX, verbose_name='', blank=True, null=True)
    capital_gain_operator = models.CharField(max_length=100, choices=INT_OPERATOR, verbose_name="Your capital gain", blank=True, null=True)
    capital_gain_value = models.IntegerField(verbose_name='', blank=True, null=True)
    capital_loss_operator = models.CharField(max_length=100, choices=INT_OPERATOR, verbose_name="Your capital loss", blank=True, null=True)
    capital_loss_value = models.IntegerField(verbose_name='', blank=True, null=True)
    hours_per_week_operator = models.CharField(max_length=100, choices=INT_OPERATOR, verbose_name="Your hours per week", blank=True, null=True)
    hours_per_week_value = models.IntegerField(verbose_name='', blank=True, null=True)
    native_country_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name="Your native country", blank=True, null=True)
    native_country_value = models.CharField(max_length=100, choices=CHOICE_NATIVE_COUNTRY, verbose_name='', blank=True, null=True)
    target_output = models.BooleanField()
    counter_example = GenericRelation(CounterExampleValue, content_type_field='contest_content_type', object_id_field='contest_object_id')
    valid_proportion = GenericRelation(ValidProportion)
    rbm = GenericRelation(RuleBasedModel)
    rbm_negative = GenericRelation(RuleBasedModel)



class AdultContestRelative(models.Model):
    file = models.ForeignKey(AdultFile, on_delete=models.CASCADE)
    age_operator = models.CharField(max_length=100, choices=INT_OPERATOR, verbose_name="Your age", blank=True,
                                    null=True)
    workclass_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name="Your workclass",
                                          blank=True, null=True)
    education_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name="Your education",
                                          blank=True, null=True)
    education_num_operator = models.CharField(max_length=100, choices=INT_OPERATOR,
                                              verbose_name="Your education number", blank=True, null=True)
    marital_status_operator = models.CharField(max_length=100, choices=CATE_OPERATOR,
                                               verbose_name="Your marital status", blank=True, null=True)
    occupation_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name="Your occupation",
                                           blank=True, null=True)
    relationship_operator = models.CharField(max_length=100, choices=CATE_OPERATOR,
                                             verbose_name="Your relationship status", blank=True, null=True)
    race_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name="Your race", blank=True,
                                     null=True)
    sex_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name="Your sex", blank=True,
                                    null=True)
    capital_gain_operator = models.CharField(max_length=100, choices=INT_OPERATOR, verbose_name="Your capital gain",
                                             blank=True, null=True)
    capital_loss_operator = models.CharField(max_length=100, choices=INT_OPERATOR, verbose_name="Your capital loss",
                                             blank=True, null=True)
    hours_per_week_operator = models.CharField(max_length=100, choices=INT_OPERATOR,
                                               verbose_name="Your hours per week", blank=True, null=True)
    native_country_operator = models.CharField(max_length=100, choices=CATE_OPERATOR,
                                               verbose_name="Your native country", blank=True, null=True)
    target_plaintiff = models.BooleanField()
    output_relative = models.BooleanField()
    counter_example = GenericRelation(CounterExampleRelative, content_type_field='contest_content_type', object_id_field='contest_object_id')
    valid_proportion = GenericRelation(ValidProportion)
    rbm = GenericRelation(RuleBasedModel)
    rbm_negative = GenericRelation(RuleBasedModel)
