from django.forms import ModelForm
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, Submit

from adult.models import *
from mladvocate.signals import convert_operator


class FileForm(ModelForm):

    class Meta:
        model = AdultFile
        exclude = ['y_pred', 'y_true']

    def __init__(self, *args, **kwargs):
        super(FileForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.form_id = 'id_intake_form'
        self.helper.form_method = 'POST'
        self.helper.form_tag = True
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Div(
                Div('age', 'race', 'marital_status',
                    'education', 'hours_per_week', 'occupation',
                    'capital_loss', css_class='col-md-6'),
                Div('sex', 'native_country', 'relationship',
                    'education_num', 'workclass', 'capital_gain',
                     css_class='col-md-6'),
                css_class='row'
            ),
            Div(
                Div(Submit('save', 'Save and continue'), css_class='col-md-12'), css_class='row'
            )
        )


class ContestValueForm(ModelForm):
    class Meta:
        model = AdultContestValue
        exclude = ['file', 'target_output']

    def __init__(self, *args, **kwargs):
        file = kwargs.pop('file', None)
        super(ContestValueForm, self).__init__(*args, **kwargs)
        self.file = file

    helper = FormHelper()
    # helper.form_class = 'form-horizontal'
    helper.label_class = 'col-lg-12'
    helper.field_class = 'col-lg-12'
    helper.form_show_labels = False


    def clean(self, *args, **kwargs):
        """
        In here you can validate the two fields
        raise ValidationError if you see anything goes wrong.
        for example if you want to make sure that field1 != field2
        """
        cleaned_data = super().clean()
        f_names = [f.name for f in AdultContestValue._meta.get_fields()]
        model_fields = {name.replace('_operator', '').replace('_value', '')
                        for name in f_names if '_operator' in name or '_value' in name}
        for field in list(model_fields):
            field_op = cleaned_data[field + '_operator']
            field_val = cleaned_data[field + '_value']
            if (field_op is None) != (field_val is None):
                self._errors[field + '_value'] = ["Both operator and value should be filled or left empty."]  # Will raise a error message
            if ((field_op is not None) and
                not convert_operator(field_op)(self.file.__dict__[field], field_val)):
                self._errors[field + '_value'] = ["Your own file should satisfy your constraint"]  # Will raise a error message
        return cleaned_data


class ContestRelativeForm(ModelForm):
    class Meta:
        model = AdultContestRelative
        exclude = ['file', 'target_plaintiff', 'output_relative']

    helper = FormHelper()
    # helper.form_class = 'form-horizontal'
    helper.label_class = 'col-lg-12'
    helper.field_class = 'col-lg-12'
    helper.form_show_labels = False

    def __init__(self, *args, **kwargs):
        file = kwargs.pop('file', None)
        super(ContestRelativeForm, self).__init__(*args, **kwargs)
        self.file = file
