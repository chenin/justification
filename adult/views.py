import pandas as pd

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.views.generic.edit import CreateView
from django.views.generic.detail import DetailView
from keras import backend as K

from adult.forms import FileForm, ContestValueForm, ContestRelativeForm
from adult.models import *
from generate_justifications.dataset.load_data_and_model import load_model


class FileCreateView(CreateView):
    def get(self, request, *args, **kwargs):
        context = {'form': FileForm(), 'bad_target': "<50k"}
        return render(request, 'file_form.html', context)

    def post(self, request, *args, **kwargs):
        form = FileForm(request.POST)
        if form.is_valid():
            file = AdultFile(**form.cleaned_data)
            file_data = file.__dict__
            pandas_dict = {k: [v] for k, v in file_data.items() if k not in ['id', '_state']}
            x = pd.DataFrame.from_dict(pandas_dict)
            K.clear_session()
            mod = load_model('adult')
            file.y_pred = bool(mod(x))
            K.clear_session()
            file.save()
            return HttpResponseRedirect('/adult/contest_type/' + str(file.pk))
        return render(request, 'file_form.html', {'form': form})


def get_contest_type(request, file_pk):
    file = AdultFile.objects.get(pk=file_pk)
    return render(request, 'adult_contest_type.html', {'file_pk': file_pk, 'file': file.__dict__})


class ContestValueCreateView(CreateView):
    def get(self, request, *args, **kwargs):
        file = AdultFile.objects.get(pk=self.kwargs['file_pk'])
        context = {'form': ContestValueForm(file=file), 'file': file.__dict__}
        return render(request, 'adult_contest_value_form.html', context)

    def post(self, request, *args, **kwargs):
        file = AdultFile.objects.get(pk=self.kwargs['file_pk'])
        form = ContestValueForm(request.POST, file=file)
        if form.is_valid():
            contest_value = form.save(commit=False)
            contest_value.file = file
            contest_value.target_output = 1 - file.__dict__['y_pred']
            contest_value.save()
            return HttpResponseRedirect('/value_justification/adult/' + str(contest_value.pk))
        return render(request, 'adult_contest_value_form.html', {'form': form, 'file': file.__dict__})


class ContestRelativeCreateView(CreateView):
    def get(self, request, *args, **kwargs):
        file = AdultFile.objects.get(pk=self.kwargs['file_pk'])
        context = {'form': ContestRelativeForm(file=file), 'file': file.__dict__}
        return render(request, 'adult_contest_relative_form.html', context)

    def post(self, request, *args, **kwargs):
        file = AdultFile.objects.get(pk=self.kwargs['file_pk'])
        form = ContestRelativeForm(request.POST, file=file)
        if form.is_valid():
            contest_relative = form.save(commit=False)
            contest_relative.file = file
            contest_relative.target_plaintiff = 1 - file.__dict__['y_pred']
            contest_relative.output_relative = file.__dict__['y_pred']
            contest_relative.save()
            return HttpResponseRedirect('/relative_justification/adult/' + str(contest_relative.pk))
        return render(request, 'adult_contest_relative_form.html', {'form': form, 'file': file.__dict__})

