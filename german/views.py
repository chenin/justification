import pandas as pd

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.views.generic.edit import CreateView
from django.views.generic.detail import DetailView
from django.shortcuts import redirect

from german.forms import ContestValueForm, ContestRelativeForm
from german.models import *
from generate_justifications.dataset.load_data_and_model import load_model


def welcome_page(request):
    return render(request, 'home.html')


class ContestValueCreateView(CreateView):
    def get(self, request, *args, **kwargs):
        file = GermanFile.objects.get(pk=self.kwargs['file_pk'])
        context = {'form': ContestValueForm(file=file), 'file': file.__dict__}
        return render(request, 'german_contest_value_form.html', context)

    def post(self, request, *args, **kwargs):
        file = GermanFile.objects.get(pk=self.kwargs['file_pk'])
        form = ContestValueForm(request.POST, file=file)
        if form.is_valid():
            contest_value = form.save(commit=False)
            contest_value.file = file
            contest_value.target_output = 1 - file.__dict__['y_pred']
            contest_value.save()
            return HttpResponseRedirect('/value_justification/german/' + str(contest_value.pk))
        return render(request, 'german_contest_value_form.html', {'form': form, 'file': file.__dict__})


class ContestRelativeCreateView(CreateView):
    def get(self, request, *args, **kwargs):
        plaintiff_file = GermanFile.objects.get(pk=self.kwargs['file_pk'])
        relative_file = GermanFile.objects.get(pk=request.session['relative_file_pk'])
        context = {'form': ContestRelativeForm(plaintiff_file=plaintiff_file, relative_file=relative_file),
                   'plaintiff_file': plaintiff_file.__dict__,
                   'relative_file': relative_file}
        return render(request, 'german_contest_relative_form.html', context)

    def post(self, request, *args, **kwargs):
        plaintiff_file = GermanFile.objects.get(pk=self.kwargs['file_pk'])
        relative_file = GermanFile.objects.get(pk=request.session['relative_file_pk'])
        form = ContestRelativeForm(request.POST, plaintiff_file=plaintiff_file, relative_file=relative_file)
        if form.is_valid():
            contest_relative = form.save(commit=False)
            contest_relative.plaintiff_file = plaintiff_file
            contest_relative.relative_file = relative_file
            contest_relative.target_plaintiff = 1 - plaintiff_file.__dict__['y_pred']
            contest_relative.output_relative = relative_file.__dict__['y_pred']
            contest_relative.save()
            return HttpResponseRedirect('/relative_justification/german/' + str(contest_relative.pk))
        return render(request, 'german_contest_relative_form.html', {'form': form, 'plaintiff_file': plaintiff_file.__dict__, 'relative_file': relative_file})
