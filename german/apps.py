from django.apps import AppConfig


class GermanConfig(AppConfig):
    name = 'german'

class ActivityAppConfig(AppConfig):
    name = 'german'

    def ready(self):
        import mladvocate.signals
