from django.contrib import admin
from german.models import *

admin.site.register(GermanContestValue)
admin.site.register(GermanContestRelative)
admin.site.register(Counterfact)
admin.site.register(CounterfactData)
admin.site.register(KNeighbors)
admin.site.register(neighbor)

from import_export.admin import ImportExportActionModelAdmin

class TaskAdmin(ImportExportActionModelAdmin):
    model = Task

admin.site.register(Task, TaskAdmin)

class GermanFileAdmin(ImportExportActionModelAdmin):
    model = GermanFile

admin.site.register(GermanFile, GermanFileAdmin)
