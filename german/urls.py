from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

from german.views import *

urlpatterns = [
    # path('enter_file/', FileCreateView.as_view(), name='get_file'),
    # path('contest_type/<int:file_pk>/', get_contest_type, name='get_contest_type'),
    path('contest_value/<int:file_pk>/', ContestValueCreateView.as_view(), name='get_contest_value'),
    # path('contest_relative_first/<int:plaintiff_file_pk>/', RelativeFileCreateView.as_view(), name='get_contest_relative'),
    path('contest_relative_second/<int:file_pk>/', ContestRelativeCreateView.as_view()),
]
