import pandas as pd
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, Submit

from django.forms import ModelForm
from german.models import *
from mladvocate.signals import convert_operator
from mladvocate.utils import *
from generate_justifications.dataset.load_data_and_model import load_model



class ContestValueForm(ModelForm):
    class Meta:
        model = GermanContestValue
        exclude = ['file', 'target_output']

    def __init__(self, *args, **kwargs):
        file = kwargs.pop('file', None)
        super(ContestValueForm, self).__init__(*args, **kwargs)
        self.file = file

    helper = FormHelper()
    # helper.form_class = 'form-horizontal'
    helper.label_class = 'col-lg-12'
    helper.field_class = 'col-lg-12'
    helper.form_show_labels = False


    def clean(self, *args, **kwargs):
        """
        In here you can validate the two fields
        raise ValidationError if you see anything goes wrong.
        for example if you want to make sure that field1 != field2
        """
        data = super().__dict__
        cleaned_data = super().clean()
        f_names = [f.name for f in GermanContestValue._meta.get_fields()]
        model_fields = {name.replace('_operator', '').replace('_value', '')
                        for name in f_names if '_operator' in name or '_value' in name}
        for field in list(model_fields):
            field_op = cleaned_data[field + '_operator']
            field_val = cleaned_data[field + '_value']
            if (field_op is None) != (field_val is None):
                self._errors[field + '_value'] = ["L'opérateur et la valeur doivent être remplis ou laissés vides"]  # Will raise a error message
            if ((field_op is not None) and
                not convert_operator(field_op)(self.file.__dict__[field], field_val)):
                self._errors[field + '_value'] = ["Le dossier doit satisfaire cette contrainte"]  # Will raise a error message
        return cleaned_data


class ContestRelativeForm(ModelForm):
    class Meta:
        model = GermanContestRelative
        exclude = ['plaintiff_file', 'relative_file', 'target_plaintiff', 'output_relative']

    def __init__(self, *args, **kwargs):
        plaintiff_file = kwargs.pop('plaintiff_file', None)
        relative_file = kwargs.pop('relative_file', None)
        super(ContestRelativeForm, self).__init__(*args, **kwargs)
        self.plaintiff_file = plaintiff_file
        self.relative_file = relative_file


    helper = FormHelper()
    # helper.form_class = 'form-horizontal'
    helper.label_class = 'col-lg-12'
    helper.field_class = 'col-lg-12'
    helper.form_show_labels = False

    def clean(self, *args, **kwargs):
        """
        In here you can validate the two fields
        raise ValidationError if you see anything goes wrong.
        for example if you want to make sure that field1 != field2
        """
        cleaned_data = super().clean()
        f_names = [f.name for f in GermanContestRelative._meta.get_fields()]
        model_fields = {name.replace('_operator', '')
                        for name in f_names if '_operator' in name or '_value' in name}
        for field in list(model_fields):
            field_op = cleaned_data[field + '_operator']
            if field_op is not None:
                cond = convert_operator(field_op)(
                    self.plaintiff_file.__dict__[field], self.relative_file.__dict__[field]
                )
                if not cond:
                    self._errors[field + '_operator'] = ["Les dossiers doivent satisfaire cette contrainte"]  # Will raise a error message

        return cleaned_data
