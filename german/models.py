from django.db import models
from django.contrib.contenttypes.fields import GenericRelation

from mladvocate.models import *



CHOICE_CREDIT_HISTORY = (
                         ("'existing paid'", "'existing paid'"),
                         ("'no credits/all paid'", "'no credits/all paid'"),
                         ("'critical/other existing credit'", "'critical/other existing credit'"),
                         ("'all paid'", "'all paid'"),
                         ("'delayed previously'", "'delayed previously'"),
)
CHOICE_PURPOSE = (
                  ("'new car'", "'new car'"),
                  ('radio/tv', 'radio/tv'),
                  ('education', 'education'),
                  ("'domestic appliance'", "'domestic appliance'"),
                  ("'used car'", "'used car'"),
                  ('business', 'business'),
                  ('furniture/equipment', 'furniture/equipment'),
                  ('other', 'other'),
                  ('repairs', 'repairs'),
                  ('retraining', 'retraining'))
CHOICE_EMPLOYMENT = (
                     ("'1<=X<4'", "'1<=X<4'"),
                     ('unemployed', 'unemployed'),
                     ("'>=7'", "'>=7'"),
                     ("'4<=X<7'", "'4<=X<7'"),
                     ("'<1'", "'<1'"),
)
CHOICE_PERSONAL_STATUS = (
                          ("'male single'", "'male single'"),
                          ("'female div/dep/mar'", "'female div/dep/mar'"),
                          ("'male div/sep'", "'male div/sep'"),
                          ("'male mar/wid'", "'male mar/wid'"),
)
CHOICE_OTHER_PARTIES = (
    ('none', 'none'),
    ("'co applicant'", "'co applicant'"),
    ('guarantor', 'guarantor'),
)
CHOICE_PROPERTY_MAGNITUDE = (
    ('car', 'car'),
    ("'life insurance'", "'life insurance'"),
    ("'no known property'", "'no known property'"),
    ("'real estate'", "'real estate'"),
)
CHOICE_HOUSING = (
    ('own', 'own'),
    ("'for free'", "'for free'"),
    ('rent', 'rent'),
)
CHOICE_JOB = (
    ('skilled', 'skilled'),
    ("'high qualif/self emp/mgmt'", "'high qualif/self emp/mgmt'"),
    ("'unemp/unskilled non res'", "'unemp/unskilled non res'"),
    ("'unskilled resident'", "'unskilled resident'"),
)
CHOICE_FOREIGN_WORKER = (
    ('yes', 'yes'),
    ('no', 'no'),
)

TARGET_CHOICES = (
    ('1', 'good'),
    ('0', 'bad'),
)
CATE_OPERATOR = (
    ('', ''),
    ('est', 'est'),
    ("n'est pas", "n'est pas"),
)
INT_OPERATOR = (
    ('', ''),
    ('>=', '>='),
    ('<=', '<='),
)

CHOICE_HELP = (
    ('MLA_VALUES', 'MLA_VALUES'),
    ('MLA_RELATIVE', 'MLA_RELATIVE'),
    ('COUNTERFACT', 'COUNTERFACT'),
    ('K_NEIGHBORS', 'K_NEIGHBORS'),
    ('NONE', 'NONE'),
)

TYPICAL_HIGH_RISK = {
    'duration': 12,
    'credit_history': "'critical/other existing credit'",
    'purpose': "'new car'",
    'credit_amount': 939,
    'employment': "'4<=X<7'",
    'personal_status': "'male mar/wid'",
    'other_parties': 'none',
    'property_magnitude': "'real estate'",
    'age': 28,
    'housing': 'own',
    'job': 'skilled',
    'foreign_worker': 'yes',
    'y_true': 0,
    'y_pred': False
}


TYPICAL_LOW_RISK = {
    'duration': 30,
    'credit_history': "'existing paid'",
    'purpose': 'radio/tv',
    'credit_amount': 1867,
    'employment': "'>=7'",
    'personal_status': "'male single'",
    'other_parties': 'none',
    'property_magnitude': 'car',
    'age': 58,
    'housing': 'own',
    'job': 'skilled',
    'foreign_worker': 'yes',
    'y_true': 1,
    'y_pred': True
}


class GermanFile(models.Model):
    credit_amount = models.IntegerField(default=4000)
    duration = models.IntegerField(default=25)
    purpose = models.CharField(max_length=100, choices=CHOICE_PURPOSE, default=CHOICE_PURPOSE[0])
    credit_history = models.CharField(max_length=100, choices=CHOICE_CREDIT_HISTORY, default=CHOICE_CREDIT_HISTORY[0])
    property_magnitude = models.CharField(max_length=100, choices=CHOICE_PROPERTY_MAGNITUDE, default=CHOICE_PROPERTY_MAGNITUDE[0])
    housing = models.CharField(max_length=100, choices=CHOICE_HOUSING, default=CHOICE_HOUSING[0])
    employment = models.CharField(max_length=100, choices=CHOICE_EMPLOYMENT, default=CHOICE_EMPLOYMENT[0])
    job = models.CharField(max_length=100, choices=CHOICE_JOB, default=CHOICE_JOB[0])
    age = models.IntegerField(default=33)
    personal_status = models.CharField(max_length=100, choices=CHOICE_PERSONAL_STATUS, default=CHOICE_PERSONAL_STATUS[0])
    foreign_worker = models.CharField(max_length=100, choices=CHOICE_FOREIGN_WORKER, default=CHOICE_FOREIGN_WORKER[0])
    other_parties = models.CharField(max_length=100, choices=CHOICE_OTHER_PARTIES, default=CHOICE_OTHER_PARTIES[0])
    y_true = models.NullBooleanField(blank=True, null=True)
    y_pred = models.BooleanField(default=True)
    counter_example_value = GenericRelation(CounterExampleValue, related_name='file_counter_example_value', content_type_field='file_content_type', object_id_field='file_object_id')
    counter_example_plaintiff = GenericRelation(CounterExampleRelative, related_name='plaintiff_counter_example_relative', content_type_field='plaintiff_content_type', object_id_field='plaintiff_object_id')
    counter_example_relative = GenericRelation(CounterExampleRelative, related_name='relative_counter_example_relative', content_type_field='relative_content_type', object_id_field='relative_object_id')


class GermanContestValue(models.Model):
    file = models.ForeignKey(GermanFile, on_delete=models.CASCADE)
    duration_operator = models.CharField(max_length=100, choices=INT_OPERATOR, verbose_name="Duration", blank=True, null=True)
    duration_value = models.IntegerField(verbose_name='', blank=True, null=True)
    credit_history_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name="Credit History", blank=True, null=True)
    credit_history_value = models.CharField(max_length=100, choices=CHOICE_CREDIT_HISTORY, verbose_name='', blank=True, null=True)
    purpose_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name="Purpose", blank=True, null=True)
    purpose_value = models.CharField(max_length=100, choices=CHOICE_PURPOSE, verbose_name='', blank=True, null=True)
    credit_amount_operator = models.CharField(max_length=100, choices=INT_OPERATOR, verbose_name="Credit Amount", blank=True, null=True)
    credit_amount_value = models.IntegerField(verbose_name='', blank=True, null=True)
    employment_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name="Employment", blank=True, null=True)
    employment_value = models.CharField(max_length=100, choices=CHOICE_EMPLOYMENT, verbose_name='', blank=True, null=True)
    personal_status_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name="Personal Status", blank=True, null=True)
    personal_status_value = models.CharField(max_length=100, choices=CHOICE_PERSONAL_STATUS, verbose_name='', blank=True, null=True)
    other_parties_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name="Other Parties", blank=True, null=True)
    other_parties_value = models.CharField(max_length=100, choices=CHOICE_OTHER_PARTIES, verbose_name='', blank=True, null=True)
    property_magnitude_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name="Property Magnitude", blank=True, null=True)
    property_magnitude_value = models.CharField(max_length=100, choices=CHOICE_PROPERTY_MAGNITUDE, verbose_name='', blank=True, null=True)
    age_operator = models.CharField(max_length=100, choices=INT_OPERATOR, verbose_name="Age", blank=True, null=True)
    age_value = models.IntegerField(verbose_name='', blank=True, null=True)
    housing_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name="Housing", blank=True, null=True)
    housing_value = models.CharField(max_length=100, choices=CHOICE_HOUSING, verbose_name='', blank=True, null=True)
    job_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name="Job", blank=True, null=True)
    job_value = models.CharField(max_length=100, choices=CHOICE_JOB, verbose_name='', blank=True, null=True)
    foreign_worker_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name="Foreign Worker", blank=True, null=True)
    foreign_worker_value = models.CharField(max_length=100, choices=CHOICE_FOREIGN_WORKER, verbose_name='', blank=True, null=True)
    target_output = models.BooleanField()
    counter_example = GenericRelation(CounterExampleValue, content_type_field='contest_content_type', object_id_field='contest_object_id')
    valid_proportion = GenericRelation(ValidProportion)
    rbm = GenericRelation(RuleBasedModel)
    rbm_negative = GenericRelation(RuleBasedModel)


class GermanContestRelative(models.Model):
    plaintiff_file = models.ForeignKey(GermanFile, on_delete=models.CASCADE, related_name='plaintiff_file')
    relative_file = models.ForeignKey(GermanFile, on_delete=models.CASCADE, related_name='relative_file')
    duration_operator = models.CharField(max_length=100, choices=INT_OPERATOR, verbose_name="Duration", blank=True, null=True)
    credit_history_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name="Credit History", blank=True, null=True)
    purpose_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name="Purpose", blank=True, null=True)
    credit_amount_operator = models.CharField(max_length=100, choices=INT_OPERATOR, verbose_name="Credit Amount", blank=True, null=True)
    employment_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name="Employment", blank=True, null=True)
    personal_status_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name="Personal Status", blank=True, null=True)
    other_parties_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name="Other Parties", blank=True, null=True)
    property_magnitude_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name="Property Magnitude", blank=True, null=True)
    age_operator = models.CharField(max_length=100, choices=INT_OPERATOR, verbose_name="Age", blank=True, null=True)
    housing_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name="Housing", blank=True, null=True)
    job_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name="Job", blank=True, null=True)
    foreign_worker_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name="Foreign Worker", blank=True, null=True)
    target_plaintiff = models.BooleanField()
    output_relative = models.BooleanField()
    counter_example = GenericRelation(CounterExampleRelative, content_type_field='contest_content_type', object_id_field='contest_object_id')
    valid_proportion = GenericRelation(ValidProportion)
    rbm = GenericRelation(RuleBasedModel)
    rbm_negative = GenericRelation(RuleBasedModel)


class Task(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    sample1 = models.ForeignKey(GermanFile, on_delete=models.CASCADE, related_name="first")
    sample2 = models.ForeignKey(GermanFile, on_delete=models.CASCADE, related_name="second")
    sample3 = models.ForeignKey(GermanFile, on_delete=models.CASCADE, related_name="third")
    help_type = models.CharField(max_length=100, choices=CHOICE_HELP, default=CHOICE_HELP[0])
    true_answer = models.PositiveSmallIntegerField(validators=[MaxValueValidator(3)])
    user_answer = models.PositiveSmallIntegerField(validators=[MaxValueValidator(3)], blank=True, null=True)
    likert_confidence = models.PositiveSmallIntegerField(validators=[MaxValueValidator(5)], blank=True, null=True)
    comment = models.CharField(max_length=3000, blank=True, null=True)


class Counterfact(models.Model):
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    sample = models.ForeignKey(GermanFile, on_delete=models.CASCADE)


class CounterfactData(models.Model):
    counterfact = models.ForeignKey(Counterfact, on_delete=models.CASCADE)
    feature = models.CharField(max_length=300)
    old_val = models.CharField(max_length=300)
    new_val = models.CharField(max_length=300)


class KNeighbors(models.Model):
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    sample = models.ForeignKey(GermanFile, on_delete=models.CASCADE)
    percentage_one = models.FloatField(validators=[MaxValueValidator(100)])
    percentage_zero = models.FloatField(validators=[MaxValueValidator(100)])


class neighbor(models.Model):
    k_neighbors = models.ForeignKey(KNeighbors, on_delete=models.CASCADE)
    sample = models.ForeignKey(GermanFile, on_delete=models.CASCADE)
