import numpy as np
import pandas as pd
import re
import warnings
warnings.filterwarnings("ignore")
from skrules import SkopeRules


def sort_plaintiff_relative_cols(dataframe):
    c_cols = [col for col in dataframe.columns if col[-2:] == '_c']
    r_cols = [col for col in dataframe.columns if col[-2:] == '_r']
    return (c_cols, r_cols)


def paired_to_unique_dataset(full_merge):
    return full_merge.groupby(by='unique_identifier_c').first()


def justification_dataset(contestation, data, consequent, how='both'):
    """
    Select from dataset examples that contradicts the contestation
    :param contestation: Contestvalues or ContestRelative object
        Object that contains the original contestation
    :param data: pd.DataFrame
        Training dataset
    :param consequent: 'string'
        The status of the the consequent in the justification dataset: 'true', 'negative' or 'undetermined'.
    :param how: 'string', optional (default='both')
        Defines which output should be used to select the examples "pred", "true", "both" or "none"
    :return: pd.DataFrame
        A dataframe containing all the examples that contradict the contestation.
        For relative contestation, each example contains two samples one that represent the
        file of the complainant (suffixe '_c') and the other that represent the file to
        which it is compared (suffixe '_r')
    """
    if contestation.type == "values":
        if consequent == 'true':
            return data[contestation.test(data, how=how)]
        elif consequent == 'negative':
            return data[contestation.test(data, negative=True, how=how)]
        elif consequent == 'undetermined':
            return data[contestation.test_antecedent(data)]
        else:
            raise ValueError("'consequent' should be 'true', 'negative' or 'undetermined'")

    elif contestation.type == "relative":
        # creating a dataset with every pair of samples that verify the antecedent
        equal_pred = contestation.rule.select_equal_predicates()
        data['unique_identifier'] = range(len(data))
        if equal_pred:
            # if the contestation contains equal conditions, use them as key for the merging (optimization)
            merge_keys = [pred.l_index for pred in equal_pred if pred.l_index == pred.r_index]
        else:
            # otherwise full cartesian product
            data.loc[:, 'key'] = 1
            merge_keys = ['key']
        full_merge = pd.merge(data, data, on=merge_keys, suffixes=('_c', '_r'))
        full_merge = full_merge[full_merge['unique_identifier_c'] != full_merge['unique_identifier_r']]
        full_merge.set_index(['unique_identifier_c', 'unique_identifier_r'], inplace=True)
        for non_duplicated_pred in equal_pred:
            col_name = non_duplicated_pred.l_index
            full_merge[col_name + '_c'] = full_merge[col_name]
            full_merge[col_name + '_r'] = full_merge[col_name]
            del full_merge[col_name]
        try:
            del full_merge['key']
        except KeyError:
            None
        # filter samples that don't match other conditions
        c_cols, r_cols = sort_plaintiff_relative_cols(full_merge)
        if consequent == 'true':
            filter = contestation.test(full_merge[c_cols], full_merge[r_cols],
                                       how=how, suffixes=('_c', '_r'))
        elif consequent == 'negative':
            filter = contestation.test(full_merge[c_cols], full_merge[r_cols],
                                       negative=True, how=how, suffixes=('_c', '_r'))
        elif consequent == 'undetermined':
            filter = contestation.test_antecedent(
                full_merge[c_cols], full_merge[r_cols], how=how, suffixes=('_c', '_r')
            )
        else:
            raise ValueError("'consequent' should be 'true', 'negative' or 'undetermined'")
        return full_merge[filter]


def find_counter_example(contestation, data, how='both'):
    if contestation.type == 'values':
        examples = justification_dataset(contestation, data, 'negative', how=how)
    elif contestation.type == 'relative':
        paired_examples = justification_dataset(contestation, data, 'negative', how=how)
        examples = paired_to_unique_dataset(paired_examples)
    if len(examples)>0:
        return examples.sample(1)
    else:
        return None


def compute_proportion_valid(contestation, data, how='both'):
    if contestation.type == 'values':
        examples_true = justification_dataset(contestation, data, 'true', how=how)
        all_examples = justification_dataset(contestation, data, 'undetermined', how=how)
    elif contestation.type == 'relative':
        examples_true_paired = justification_dataset(contestation, data, 'true', how=how)
        all_examples_paired = justification_dataset(contestation, data, 'undetermined', how=how)
        examples_true = paired_to_unique_dataset(examples_true_paired)
        all_examples = paired_to_unique_dataset(all_examples_paired)
    else:
        raise ValueError("Contestation type should be value or relative")
    if len(all_examples) > 0:
        return round(float(len(examples_true)) / float(len(all_examples)) * 100, 2)
    else:
        return None


def compute_valid_size(contestation, data, how='both'):
    if contestation.type == 'values':
        examples_true = justification_dataset(contestation, data, 'true', how=how)
        all_examples = justification_dataset(contestation, data, 'undetermined', how=how)
    elif contestation.type == 'relative':
        examples_true_paired = justification_dataset(contestation, data, 'true', how=how)
        all_examples_paired = justification_dataset(contestation, data, 'undetermined', how=how)
        examples_true = paired_to_unique_dataset(examples_true_paired)
        all_examples = paired_to_unique_dataset(all_examples_paired)
    else:
        raise ValueError("Contestation type should be value or relative")
    if len(all_examples) > 0:
        return len(examples_true), len(all_examples)
    else:
        return None
