import copy
import numpy as np
import pandas as pd
from sklearn.tree import DecisionTreeClassifier
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import OneHotEncoder
import re
import operator
from scipy.stats import ttest_ind

from generate_justifications.justify.justification import *


def check_predicate(predicate, sample):
    assert len(sample) == 1
    feature, value, operator = predicate
    return len(sample.loc[operator(sample[feature], value)]) == 1


def find_best_predicate(samples, predicate_cand, negative):
    pareto_front = []
    for feat, val, op in predicate_cand:
        # select corresponding region
        r = samples.loc[op(samples[feat], val), 'expected']
        if len(r) > 0:
            r_mean, r_size = r.mean(), len(r)
            to_remove = []
            # filter pareto front
            for _, __, ___, size, mean, pval in pareto_front:
                if r_size > size:
                    if negative:
                        if r_mean >= mean: # only select contradictory predicates
                            to_remove.append((_, __,___, size, mean, pval))
                    else:
                        if r_mean <= mean:
                            to_remove.append((_, __,___, size, mean, pval))
            for item in to_remove:
                pareto_front.remove(item)
            if negative:
                if all([r_size >= size or r_mean >= mean for _, __, ___, size, mean, pval in pareto_front]):
                    _, pvalue = ttest_ind(samples['expected'], r)
                    pareto_front.append((feat, val, op, r_size, r_mean, pvalue))
            else:
                if all([r_size >= size or r_mean <= mean for _, __, ___, size, mean, pval in pareto_front]):
                    _, pvalue = ttest_ind(samples['expected'], r)
                    pareto_front.append((feat, val, op, r_size, r_mean, pvalue))
    return sorted(pareto_front, key=lambda x: x[5])[0]


def all_possible_splits(samples):
    categorical_features = [x for x in samples.columns if samples[x].dtype == 'object']
    other_features = [x for x in samples.columns if samples[x].dtype != 'object']
    splits = []
    for cat_feat in categorical_features:
        for val in samples[cat_feat].unique():
            splits.append((cat_feat, val, operator.eq))
    for feat in other_features:
        thresholds = np.sort(samples[feat].unique())
        for t in thresholds:
            splits.append((feat, t, operator.ge))
            splits.append((feat, t, operator.le))
            splits.append((feat, t, operator.eq))
    return splits


def operator_to_symbol(op):
    if op == operator.eq:
        return " is "
    elif op == operator.lt:
        return " < "
    elif op == operator.gt:
        return " > "
    elif op == operator.le:
        return " <= "
    elif op == operator.ge:
        return " >= "
    elif op == operator.neq:
        return " is not "
    else:
        raise ValueError("Operator not valid.")


def format_rules(rules):
    rule_tuple = [(pred[0], operator_to_symbol(pred[2]), str(pred[1])) for pred in rules]
    return (rule_tuple, rules[-1][3], rules[-1][4])

