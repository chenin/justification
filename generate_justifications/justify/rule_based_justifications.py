import copy
import numpy as np
import pandas as pd
from sklearn.tree import DecisionTreeClassifier
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import OneHotEncoder
import re
import operator
from scipy.stats import ttest_ind

from generate_justifications.justify.justification import *


def check_predicate(predicate, sample):
    assert len(sample) == 1
    feature, value, operator = predicate
    return len(sample.loc[operator(sample[feature], value)]) == 1


def find_best_predicate(samples, predicate_cand, negative):
    pareto_front = []
    for feat, val, op in predicate_cand:
        # select corresponding region
        r = samples.loc[op(samples[feat], val), 'expected']
        if len(r) > 0:
            r_mean, r_size = r.mean(), len(r)
            to_remove = []
            # filter pareto front
            for _, __, ___, size, mean, pval in pareto_front:
                if r_size > size:
                    if negative:
                        if r_mean >= mean: # only select contradictory predicates
                            to_remove.append((_, __,___, size, mean, pval))
                    else:
                        if r_mean <= mean:
                            to_remove.append((_, __,___, size, mean, pval))
            for item in to_remove:
                pareto_front.remove(item)
            if negative:
                if all([r_size >= size or r_mean >= mean for _, __, ___, size, mean, pval in pareto_front]):
                    _, pvalue = ttest_ind(samples['expected'], r)
                    pareto_front.append((feat, val, op, r_size, r_mean, pvalue))
            else:
                if all([r_size >= size or r_mean <= mean for _, __, ___, size, mean, pval in pareto_front]):
                    _, pvalue = ttest_ind(samples['expected'], r)
                    pareto_front.append((feat, val, op, r_size, r_mean, pvalue))
    return sorted(pareto_front, key=lambda x: x[5])[0]


def all_possible_splits(samples):
    categorical_features = [x for x in samples.columns if samples[x].dtype == 'object']
    other_features = [x for x in samples.columns if samples[x].dtype != 'object']
    splits = []
    for cat_feat in categorical_features:
        for val in samples[cat_feat].unique():
            splits.append((cat_feat, val, operator.eq))
    for feat in other_features:
        thresholds = np.sort(samples[feat].unique())
        for t in thresholds:
            splits.append((feat, t, operator.ge))
            splits.append((feat, t, operator.le))
            # splits.append((feat, t, operator.eq))
    return splits


def operator_to_symbol(op):
    if op == operator.eq:
        return " est "
    elif op == operator.lt:
        return " < "
    elif op == operator.gt:
        return " > "
    elif op == operator.le:
        return " <= "
    elif op == operator.ge:
        return " >= "
    elif op == operator.neq:
        return " n'est pas "
    else:
        raise ValueError("Operator not valid.")


def format_rules(rules):
    rule_tuple = [(pred[0], operator_to_symbol(pred[2]), str(pred[1])) for pred in rules]
    return (rule_tuple, rules[-1][3], rules[-1][4])


def rule_based_model(contest_sample, contestation, data, how='both'):
    samples = justification_dataset(contestation, data, 'undetermined', how='both')
    if contestation.type == "values":
        samples.loc[:, 'expected'] = contestation.test_consequent(samples, how=how)
        del samples['y_pred'], samples['y_true']
    elif contestation.type == "relative":
        c_cols, r_cols = sort_plaintiff_relative_cols(samples)
        samples.loc[:, 'expected'] = contestation.test_consequent(
            samples[c_cols], samples[r_cols], how=how, suffixes=('_c', '_r')
        )
        del samples['y_pred_c'], samples['y_pred_r'], samples['y_true_c'], samples['y_true_r']
        c_cols.remove('y_pred_c')
        c_cols.remove('y_true_c')
        samples = paired_to_unique_dataset(samples)
        # UNCOMMENT FOR FEATURES THAT ARE COMPARISON OF REFERENCE AND PLAINTIFF
        # categorical_features = [x for x in samples.columns if samples[x].dtype == 'object']
        # other_features = [x for x in samples.columns if samples[x].dtype != 'object']
        # for col in c_cols:
        #     if col in categorical_features:
        #         if col != 'expected':
        #             samples[col[:-2] + '__IS'] = (samples[col] == samples[col[:-2] + '_r'])
        #             contest_sample[col[:-2] + '__IS'] = (contest_sample[col] == contest_sample[col[:-2] + '_r'])
        #     if col in other_features:
        #         if col != 'expected':
        #             samples[col[:-2] + '__LT'] = (samples[col] < samples[col[:-2] + '_r'])
        #             contest_sample[col[:-2] + '__LT'] = (contest_sample[col] < contest_sample[col[:-2] + '_r'])
        #     del samples[col], samples[col[:-2] + '_r']
        #     del contest_sample[col], contest_sample[col[:-2] + '_r']
    if all(samples['expected'] == 1):  # all samples are in the correct class
        return ([('', '', '')], len(samples), 1), None   # return empty model
    elif all(samples['expected'] == 0):  # all samples are in the incorrect class
        return None, ([('', '', '')], len(samples), 0)

    current_mean = samples['expected'].mean()
    target_mean_positive = samples['expected'].mean() / 2.
    target_mean_negative =  (1 + samples['expected'].mean()) / 2.
    rules = []
    predicate_cand = all_possible_splits(samples[[col for col in samples.columns if col[-2:]!='_r']].drop('expected', axis=1))
    # print(predicate_cand)
    # positive RBJ
    positive_predicate_cand = [pred for pred in predicate_cand if check_predicate(pred, contest_sample)]
    current_mean = 1
    rbm_pos = []
    samples_positive = samples.copy()
    while target_mean_positive < current_mean:
        best_predicate_positive = find_best_predicate(samples_positive, positive_predicate_cand, negative=False)
        if best_predicate_positive[3] < 20:
            rbm_pos = None
            break
        else:
            rbm_pos.append(best_predicate_positive)
            feature, value, operator = best_predicate_positive[0:3]
            samples_positive = samples_positive.loc[operator(samples_positive[feature], value)]
            current_mean = best_predicate_positive[4]


    # negative RBJ
    rbm_neg = []
    current_mean = 0
    samples_negative = samples.copy()
    negative_predicate_cand = [pred for pred in predicate_cand if not check_predicate(pred, contest_sample)]
    while target_mean_positive > current_mean:
        best_predicate_negative = find_best_predicate(samples_negative, negative_predicate_cand, negative=True)
        if best_predicate_negative[3] < 20:
            rbm_pos = None
            break
        else:
            rbm_neg.append(best_predicate_negative)
            feature, value, operator = best_predicate_negative[0:3]
            samples_negative = samples_negative.loc[operator(samples_negative[feature], value)]
            current_mean = best_predicate_negative[4]
    # print(rbm_pos, rbm_neg)
    if rbm_pos is not None and rbm_neg is not None:
        return format_rules(rbm_pos), format_rules(rbm_neg)
    elif rbm_pos is not None:
        return format_rules(rbm_pos), None
    elif rbm_neg is not None:
        return None, format_rules(rbm_neg)
    else:

        return None, None


def pro_and_cons_rules(contest_sample, contestation, data, how='both'):
    samples = justification_dataset(contestation, data, 'undetermined', how='both')
    if contestation.type == "values":
        samples.loc[:, 'expected'] = contestation.test_consequent(samples, how=how)
        del samples['y_pred'], samples['y_true']
    elif contestation.type == "relative":
        c_cols, r_cols = sort_plaintiff_relative_cols(samples)
        samples.loc[:, 'expected'] = contestation.test_consequent(
            samples[c_cols], samples[r_cols], how=how, suffixes=('_c', '_r')
        )
        del samples['y_pred_c'], samples['y_pred_r'], samples['y_true_c'], samples['y_true_r']
        c_cols.remove('y_pred_c')
        c_cols.remove('y_true_c')
        samples = paired_to_unique_dataset(samples)
        samples = samples[[col for col in samples.columns if col[-2:]=='_c' or col=='expected']]
        samples.rename(columns={col: (col[:-2] if col!='expected' else 'expected') for col in samples.columns}, inplace=True)
        # UNCOMMENT FOR FEATURES THAT ARE COMPARISON OF REFERENCE AND PLAINTIFF
        # categorical_features = [x for x in samples.columns if samples[x].dtype == 'object']
        # other_features = [x for x in samples.columns if samples[x].dtype != 'object']
        # for col in c_cols:
        #     if col in categorical_features:
        #         if col != 'expected':
        #             samples[col[:-2] + '__IS'] = (samples[col] == samples[col[:-2] + '_r'])
        #             contest_sample[col[:-2] + '__IS'] = (contest_sample[col] == contest_sample[col[:-2] + '_r'])
        #     if col in other_features:
        #         if col != 'expected':
        #             samples[col[:-2] + '__LT'] = (samples[col] < samples[col[:-2] + '_r'])
        #             contest_sample[col[:-2] + '__LT'] = (contest_sample[col] < contest_sample[col[:-2] + '_r'])
        #     del samples[col], samples[col[:-2] + '_r']
        #     del contest_sample[col], contest_sample[col[:-2] + '_r']
    if all(samples['expected'] == 1):  # all samples are in the correct class
        return None, None   # return empty model
    elif all(samples['expected'] == 0):  # all samples are in the incorrect class
        return None, None   # return empty model

    current_mean = samples['expected'].mean().copy()
    target_mean_model = copy.copy(current_mean / 2.)
    target_mean_challenger = copy.copy((1 + current_mean) / 2.)
    rules = []
    predicate_cand = all_possible_splits(samples[[col for col in samples.columns if col[-2:]!='_r']].drop('expected', axis=1))
    # print(predicate_cand)
    # challenge side RBJ
    predicate_cand = [pred for pred in predicate_cand if check_predicate(pred, contest_sample)]
    current_mean = 0
    rbm_challenger = []
    samples_challenger = samples.copy()
    while target_mean_challenger > current_mean:
        # print("target: ", target_mean_challenger, "current", current_mean)
        best_predicate_challenger = find_best_predicate(samples_challenger, predicate_cand, negative=True)
        if best_predicate_challenger[5] > .2:
            # print("No model just found", best_predicate_challenger)
            rbm_challenger = None
            break
        else:
            # print("Adding a rule", best_predicate_challenger)
            rbm_challenger.append(best_predicate_challenger)
            feature, value, operator = best_predicate_challenger[0:3]
            samples_challenger = samples_challenger.loc[operator(samples_challenger[feature], value)]
            current_mean = best_predicate_challenger[4]

    # model side RBJ
    rbm_model = []
    current_mean = 1
    samples_model = samples.copy()
    while target_mean_model < current_mean:
        # print("target: ", target_mean_model, "current", current_mean)
        best_predicate_model = find_best_predicate(samples_model, predicate_cand, negative=False)
        if best_predicate_model[5] > .2:
            # print("No model just found", best_predicate_model)
            rbm_model = None
            break
        else:
            # print("Adding a rule", best_predicate_model)
            rbm_model.append(best_predicate_model)
            feature, value, operator = best_predicate_model[0:3]
            samples_model = samples_model.loc[operator(samples_model[feature], value)]
            current_mean = best_predicate_model[4]
    # print(rbm_challenger, rbm_model)
    if rbm_challenger is not None and rbm_model is not None:
        return format_rules(rbm_challenger), format_rules(rbm_model)
    elif rbm_challenger is not None:
        return format_rules(rbm_challenger), None
    elif rbm_model is not None:
        return None, format_rules(rbm_model)
    else:
        return None, None
