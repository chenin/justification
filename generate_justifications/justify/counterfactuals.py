import numpy as np
import pandas as pd
import datetime as dt

from generate_justifications.dataset.german_simple import *

def tabular_feature_similarity_to_x(feature_serie, x, col_std=1):
    if feature_serie.dtype in ['int16', 'int32', 'int64', 'float16', 'float32', 'float64']:
        # L1 norm
        dist = abs(feature_serie - x) / col_std
        return 1 / (1 + dist)
    if type(feature_serie.head(1).values[0]) in [dt.date]:
        # L1 norm
        dist = abs(feature_serie - x) / col_std
        return 1 / (1 + dist.astype(float))
    if type(feature_serie.head(1).values[0]) == set:
        # Jaccard similarity
        return feature_serie.apply(lambda item: len(item.intersection(x)) / float(len(item.union(x))))
    else:
        # Goodall similarity
        eq_counts = sum(feature_serie == x)
        N_tot = len(feature_serie)
        return (feature_serie == x) * (eq_counts * (eq_counts - 1)) / (N_tot * (N_tot - 1))


def pop_similarity_to_x(samples, x, data):
    dist_df = pd.DataFrame()
    std = data.std()
    # compute column-wise similarity
    for col in samples.columns:
        dist_df[col] = tabular_feature_similarity_to_x(samples[col],
                                                       x[col].values[0],
                                                       std.get(col, None))
    return dist_df.mean(axis=1)


def simulate_uniform_draw_counts(n_draws, support_size):
    p = {}
    for k in range(1, support_size):
        p[k] = np.random.binomial(n_draws - sum(p.values()), 1. / (support_size - k))
    return p


def replace_col_values(original_df, new_val_df, col_array):
    """
    Exchange values from original_df with values of new_val_df.
    This function can be used in different ways.
    If len(new_val_df) is one,
    then the same values are use for every row of original_df.
    Else (original_df and new_val_df should be of the same size),
    each value of original_df is replaced by a randomly picked
    value from new_val_df.
    If col_array contains only one element,
    only one column is replaced everywhere in original_df.
    Else len(col_array) should be the size of original_df
    for each row of orignal_df a different combination of
    columns is used.
    :param original_df: dataframe containing values that will be replaced
    :param new_val_df: dataframe containing values that replaces the original ones
    :param col_array: list of set of column(s) to be exchanged
    :return: dataframe with replaced values
    """
    # creating a joined dataframe that gathers every values to be replaced
    index_save = original_df.index
    left = original_df.copy()
    right = new_val_df.copy()
    if len(right) == 1:
        left['key'] = 1
        right['key'] = 1
        # cartesian product
        joined = pd.merge(
            left, right, on='key', suffixes=('', '_new')
        ).drop('key', axis=1)
        left.drop('key', axis=1, inplace=True)
        right.drop('key', axis=1, inplace=True)
    elif len(left) == len(right):
        # one to one matching with random permutation
        right.index = np.random.permutation(right.index)
        joined = left.join(right, rsuffix='_new')
    else:
        raise ValueError("new_values_df should be of size 1 OR of size len(original_df)")
    if len(col_array) == 1:
        # when one set of columns is specified, change this set for every row
        cols_tuple = tuple(col_array[0])
        new_cols_tuple = [c + '_new' for c in cols_tuple]
        joined.loc[:, cols_tuple] = joined.loc[:, new_cols_tuple].values
    elif len(col_array) == len(joined):
        # one set of columns is used for each row
        for cols in np.unique(np.array(col_array)):
            sel_bool_array = (np.array(col_array) == cols)
            cols_tuple = tuple(cols)
            new_cols_tuple = [str(c) + '_new' for c in cols_tuple]
            joined.loc[sel_bool_array, cols_tuple] = joined.loc[sel_bool_array, new_cols_tuple].values
    for col in left.columns:
        joined.drop(str(col) + '_new', axis=1, inplace=True)
    return joined.set_index(index_save)


def perturb_one_sample_w_pop(data, model, x_e, size=20,
                             n_col_distrib='normal',
                             normal_sigma=1):
    # create the list of variables to be replaced (list of sets)
    var_ordered = data.columns
    M = np.array([var_ordered] * size)
    if n_col_distrib == 'uniform':
        n_col_modified_count = simulate_uniform_draw_counts(size, len(var_ordered))  # simulate uniform draw
    if n_col_distrib == 'normal':
        n_col_modified_count = simulate_normal_draw_counts(size, len(var_ordered), normal_sigma)
    for n_cols_modified, counts in n_col_modified_count.items():
        if counts > 0:
            # create a bool row with n_cols_modified True and the rest as False
            # False / True encodes the columns that remains in the list
            bool_m = [False] * (len(var_ordered) - n_cols_modified) + [True] * n_cols_modified
            sel_array = np.array([bool_m] * counts)
            # suffle to simulate a uniform drawing
            bool_array_sel = shuffle_along_axis(sel_array, axis=1)
            # add "False" element for unprotected variables
            try:
                all_sel_index = np.concatenate([all_sel_index, bool_array_sel], axis=0)
            except:
                all_sel_index = bool_array_sel
    col_array = [set(M[i, ~idx]) for (i, idx) in enumerate(all_sel_index)]
    sub_population = data.sample(size, replace=True)
    samples = replace_col_values(sub_population, x_e, col_array)
    if len(pd.merge(x_e[FEATURES_LIST], samples[FEATURES_LIST], how='inner')) > 0:
        samples = pd.concat([samples, x_e], axis=0)
        samples.drop_duplicates(keep=False, inplace=True)
    output = model(samples)
    samples.loc[:, 'y_pred'] = output
    samples = samples.set_index(pd.Index([x_e.index.values[0]] * len(samples)))
    return samples


def simulate_normal_draw_counts(n_draws, support_size, sigma):
    simu = np.unique(abs(np.random.normal(0, sigma, size=n_draws).astype(int)), return_counts=True)
    counts = dict(zip(simu[0] + 1, simu[1]))
    for key in list(counts.keys()).copy():
        if key > support_size:
            counts[1] += counts[key]
            del counts[key]
    return counts


def shuffle_along_axis(a, axis):
    idx = np.random.rand(*a.shape).argsort(axis=axis)
    return np.take_along_axis(a, idx, axis=axis)


def find_cf(data, model, point_interest):
    samples = perturb_one_sample_w_pop(data[FEATURES_LIST], model, point_interest, size=100, normal_sigma=1)
    if len(samples[samples['y_pred'] != point_interest['y_pred'].values[0]]) == 0:
        print("No found")
        sigma = 1
        while len(samples[samples['y_pred'] != point_interest['y_pred'].values[0]]) <= 0:
            sigma *= 1.1
            print("sigma", sigma)
            samples = perturb_one_sample_w_pop(data, model, point_interest, size=100, normal_sigma=sigma)
        samples['sim'] = pop_similarity_to_x(samples[FEATURES_LIST], point_interest[FEATURES_LIST], data[FEATURES_LIST])
        cf_final = samples[samples['y_pred'] != point_interest['y_pred'].values[0]].sort_values('sim').tail(1)
        return cf_final.drop('sim', axis=1)
    else:
        samples['sim'] = pop_similarity_to_x(samples[FEATURES_LIST], point_interest[FEATURES_LIST], data[FEATURES_LIST])
        cf_final = samples[samples['y_pred'] != point_interest['y_pred'].values[0]].sort_values('sim').tail(1)
        return cf_final.drop('sim', axis=1)