import os
import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import RandomizedSearchCV
from joblib import dump, load
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import OneHotEncoder

from django.conf import settings
from joblib import dump, load
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import OneHotEncoder
from keras.wrappers.scikit_learn import KerasRegressor
from keras.models import Sequential
from keras.layers import Dense
from keras import backend as K

try:
    BASE_DIR = settings.BASE_DIR
except:
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))


def adult_load_model():
    features_list = ['age', 'workclass', 'education', 'education_num',
       'marital_status', 'occupation', 'relationship', 'race', 'sex',
       'capital_gain', 'capital_loss', 'hours_per_week', 'native_country']
    return lambda x: load(
    os.path.join(BASE_DIR, "./generate_justifications/dataset/adult_neural_network.joblib")).predict(x[features_list]) > .5


def adult_load_data_and_pred():
    data = pd.read_csv(os.path.join(BASE_DIR, "./generate_justifications/dataset/adult.csv"))
    data.drop('fnlwgt', axis=1, inplace=True)
    # preprocess
    data.replace('?', np.nan, inplace=True)
    data.dropna(inplace=True)
    data.replace(['Divorced', 'Married-AF-spouse',
                  'Married-civ-spouse', 'Married-spouse-absent',
                  'Never-married', 'Separated', 'Widowed'],
                 ['divorced', 'married', 'married', 'married',
                  'not married', 'not married', 'not married'], inplace=True)
    data['y_true'] = (data['Income']=='>50K').astype(int)
    del data['Income']
    K.clear_session()
    mod = adult_load_model()
    data['y_pred'] = mod(data)
    K.clear_session()
    return data


def keras_nn():
    classifier = Sequential()
    classifier.add(Dense(activation="relu", input_dim=99, units=12, kernel_initializer="uniform"))
    classifier.add(Dense(activation="relu", units=12, kernel_initializer="uniform"))
    classifier.add(Dense(activation="sigmoid", units=1, kernel_initializer="uniform"))
    classifier.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])
    return classifier


def adult_load_data_and_pred_train():
    # load dataset
    data = pd.read_csv("./generate_justifications/dataset/adult.csv")
    # preprocess
    data.replace('?', np.nan, inplace=True)
    data.dropna(inplace=True)
    data.drop('fnlwgt', axis=1, inplace=True)
    data.replace(['Divorced', 'Married-AF-spouse',
                  'Married-civ-spouse', 'Married-spouse-absent',
                  'Never-married', 'Separated', 'Widowed'],
                 ['divorced', 'married', 'married', 'married',
                  'not married', 'not married', 'not married'], inplace=True)
    data['y_true'] = (data['Income']=='>50K').astype(int)
    y_true = data['Income'].copy()
    del data['Income']

    validation = data.sample(100)
    data = data.loc[~data.index.isin(validation.index)]

    X, y = data.drop('y_true', axis=1), data['y_true']

    # encoding categorical dataset
    categorical_features = [x for x in data.columns if data[x].dtype=='object']
    categorical_transformer = Pipeline(steps=[
        ('onehot', OneHotEncoder(handle_unknown='ignore'))])
    preprocessor = ColumnTransformer(
        transformers=[('cat', categorical_transformer, categorical_features)],
        remainder='passthrough'
    )

    # encapsulate Keras model for sklearn pipeline
    neuralnet = KerasRegressor(build_fn=keras_nn, verbose=0)

    # Define pipeline
    clf = Pipeline(steps=[('preprocessor', preprocessor),
                          ('classifier', neuralnet)])

    class_weight = {1: sum(y==0), 0: sum(y==1)}  # to cope with class imbalance

    parameters = {
        'classifier__batch_size' : 32,
        'classifier__class_weight' : class_weight,
        'classifier__epochs' : 200,
    }

    clf.set_params(**parameters).fit(X, y)
    print("Validation accuracy: ", ((clf.predict(validation) > .5) == validation['y_true']).mean())
    dump(clf, os.path.join(BASE_DIR, "./generate_justifications/dataset/adult_neural_network.joblib"))
    data['y_pred'] = clf.predict(X) > .5
    return data
