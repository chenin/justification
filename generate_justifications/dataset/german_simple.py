import os
import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.neighbors import KNeighborsClassifier
from joblib import dump, load
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import OneHotEncoder

from django.conf import settings

try:
    BASE_DIR = settings.BASE_DIR
except:
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

FEATURES_LIST = ['duration', 'credit_history', 'purpose', 'credit_amount', 'employment',
                 'personal_status', 'other_parties', 'property_magnitude', 'age', 'housing', 'job',
                 'foreign_worker']

CLASS_NAMES = ['haut', 'faible']


def german_load_model():
    return lambda x: load(
                    os.path.join(BASE_DIR, './generate_justifications/dataset/german_simple_knn.joblib')).predict_proba(x[FEATURES_LIST])[:, 1] > .66


def get_k_neighbors(sample):
    mod = load(os.path.join(BASE_DIR, './generate_justifications/dataset/german_simple_knn.joblib'))
    x_t = mod.steps[0][1].transform(sample[FEATURES_LIST])
    kn = mod.steps[1][1].kneighbors(x_t)[1][0]
    return kn


def german_load_data_and_pred():
    data = pd.read_csv(os.path.join(BASE_DIR, "./generate_justifications/dataset/dataset_31_credit-g.csv"))
    features = ['duration', 'credit_history', 'purpose', 'credit_amount', 'employment',
     'personal_status', 'other_parties', 'property_magnitude', 'age', 'housing', 'job',
     'foreign_worker']
    data = data[features + ['class']]
    data['y_true'] = (data['class'] == 'good').astype(int)
    del data['class']
    mod = german_load_model()
    data['y_pred'] = mod(data)
    return data


def german_load_data_and_pred_train():
    data = pd.read_csv("./generate_justifications/dataset/dataset_31_credit-g.csv")
    data = data[FEATURES_LIST + ['class']]
    data['y_true'] = (data['class']=='good').astype(int)
    del data['class']

    # restricted feature liste

    # encoding categorical dataset
    categorical_features = [x for x in data.columns if data[x].dtype=='object']
    categorical_transformer = Pipeline(steps=[
        ('onehot', OneHotEncoder(handle_unknown='ignore'))])
    preprocessor = ColumnTransformer(
        transformers=[('cat', categorical_transformer, categorical_features)],
        remainder='passthrough'
    )

    # Append classifier to preprocessing pipeline.
    # Now we have a full prediction pipeline.
    clf = Pipeline(steps=[('preprocessor', preprocessor),
                          ('classifier', KNeighborsClassifier())])

    parameters = {'classifier__n_neighbors': range(2, 18)}

    X, y = data.drop('y_true', axis=1), data['y_true']

    pip = GridSearchCV(clf, parameters, cv=5)
    pip.fit(X, y)
    mod = pip.best_estimator_
    dump(mod, os.path.join(BASE_DIR, './generate_justifications/dataset/german_simple_knn.joblib'))
    data['y_pred'] = (mod.predict_proba(X)[:, 1] > .66)
    return data
