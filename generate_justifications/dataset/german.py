import os
import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import RandomizedSearchCV
from joblib import dump, load
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import OneHotEncoder

from django.conf import settings

try:
    BASE_DIR = settings.BASE_DIR
except:
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))


def german_load_model():
    features_list = ['checking_status', 'duration', 'credit_history', 'purpose',
       'credit_amount', 'savings_status', 'employment',
       'installment_commitment', 'personal_status', 'other_parties',
       'residence_since', 'property_magnitude', 'age',
       'other_payment_plans', 'housing', 'existing_credits', 'job',
       'num_dependents', 'own_telephone', 'foreign_worker']
    return lambda x: load(
                    os.path.join(BASE_DIR, './generate_justifications/dataset/random_forest_trained_model.joblib')).predict(x[features_list])


def german_load_data_and_pred():
    data = pd.read_csv(os.path.join(BASE_DIR, "./generate_justifications/dataset/dataset_31_credit-g.csv"))
    data['y_true'] = (data['class'] == 'good').astype(int)
    del data['class']
    mod = german_load_model()
    data['y_pred'] = mod(data)
    return data


def german_load_data_and_pred_train():
    data = pd.read_csv("./generate_justifications/dataset/dataset_31_credit-g.csv")
    data['y_true'] = (data['class']=='good').astype(int)
    del data['class']
    # encoding categorical dataset
    categorical_features = [x for x in data.columns if data[x].dtype=='object']
    categorical_transformer = Pipeline(steps=[
        ('onehot', OneHotEncoder(handle_unknown='ignore'))])
    preprocessor = ColumnTransformer(
        transformers=[('cat', categorical_transformer, categorical_features)],
        remainder='passthrough'
    )

    # Append classifier to preprocessing pipeline.
    # Now we have a full prediction pipeline.
    clf = Pipeline(steps=[('preprocessor', preprocessor),
                          ('classifier', RandomForestClassifier(class_weight='balanced_subsample'))])

    parameters = {'classifier__n_estimators': np.linspace(1, 200, 20).astype(int),
                  'classifier__max_leaf_nodes': np.linspace(2, 200, 40).astype(int),
                  'classifier__max_features': [2],
                  }

    X, y = data.drop('y_true', axis=1), data['y_true']

    pip = RandomizedSearchCV(clf, parameters, cv=5)
    pip.fit(X, y)
    mod = pip.best_estimator_
    dump(mod, './random_forest_trained_model.joblib')
    data['y_pred'] = mod.predict(X)
    return data
