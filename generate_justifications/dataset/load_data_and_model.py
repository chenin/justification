import os
import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import RandomizedSearchCV
from joblib import dump, load
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import OneHotEncoder

from django.conf import settings

from generate_justifications.dataset.german import *
from generate_justifications.dataset import german_simple as gs
from generate_justifications.dataset import adult_simple as ads
from generate_justifications.dataset.adult import *
from generate_justifications.dataset.compas import *


def load_model(dataset):
    if dataset=='german':
        return german_load_model()
    if dataset=='german_simple':
        return gs.german_load_model()
    if dataset=='adult':
        return adult_load_model()
    if dataset=='adult_simple':
        return ads.adult_load_model()
    if dataset=='compas':
        return compas_load_model()


def load_data_and_pred(dataset):
    if dataset=='german':
        return german_load_data_and_pred()
    if dataset=='german_simple':
        return gs.german_load_data_and_pred()
    if dataset=='adult':
        return adult_load_data_and_pred()
    if dataset=='adult_simple':
        return ads.adult_load_data_and_pred()
    if dataset=='compas':
        return compas_load_data_and_pred()


def load_data_and_pred_train(dataset):
    if dataset=='german':
        return german_load_data_and_pred_train()
    if dataset=='german_simple':
        return gs.german_load_data_and_pred_train()
    if dataset=='adult':
        return adult_load_data_and_pred_train()
    if dataset=='adult_simple':
        return ads.adult_load_data_and_pred_train()
    if dataset=='compas':
        return compas_load_data_and_pred_train()



