import os
import pandas as pd
import numpy as np
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.pipeline import Pipeline
from sklearn.compose import ColumnTransformer
from sklearn.preprocessing import OneHotEncoder
from joblib import dump, load
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import OneHotEncoder

from django.conf import settings

try:
    BASE_DIR = settings.BASE_DIR
except:
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))


def compas_load_model():
    features_list = ["sex", "age", "race", "priors_count", "c_charge_desc", "charge_type", "charge_gravity"]
    return lambda x: load(
                    os.path.join(BASE_DIR, './generate_justifications/dataset/compas_knn.joblib')).predict(x[features_list])


def compas_load_model_proba():
    features_list = ["sex", "age", "race", "priors_count", "c_charge_desc", "charge_type", "charge_gravity"]
    return lambda x: load(
                    os.path.join(BASE_DIR, './generate_justifications/dataset/compas_knn.joblib')).predict_proba(x[features_list])


def compas_load_data_and_pred():
    data = pd.read_csv(os.path.join(BASE_DIR, "./generate_justifications/dataset/compas.csv"))
    # preprocessing
    data["fu_time"] = data['end'] - data['start']
    data = data.groupby('id').agg({
        'sex': 'first', 'age': 'first', 'race': 'first', 'priors_count': 'first',
        'c_charge_degree': 'first', 'c_charge_desc': 'first', 'is_recid': 'first', 'fu_time': 'sum' })
    data = data.loc[(data['is_recid'] == 1) | (data['fu_time'] > 730)]
    data = data.dropna()
    data.drop(['fu_time'], inplace=True, axis=1)

    # remove unfrequent charge_degrees
    counts = data.groupby('c_charge_degree').count()
    frequent_degrees = list(counts[counts['age'] > 10].index)
    data = data.loc[data['c_charge_degree'].apply(lambda x: x in frequent_degrees)]
    data['charge_type'] = data['c_charge_degree'].apply(lambda x: x[1])
    data.replace({'charge_type': {'F': 'Felony', 'M': 'Misdemeanor'}}, inplace=True)
    data['charge_gravity'] = data['c_charge_degree'].apply(lambda x: int(x[2]) if x[2] != 'O' else 3)
    del data['c_charge_degree']
    data['y_true'] = data['is_recid']
    del data['is_recid']
    mod = compas_load_model()
    data['y_pred'] = mod(data)
    return data


def compas_load_data_and_pred_train():
    data = pd.read_csv("./generate_justifications/dataset/compas.csv")   # cox-parsed.csv file from Propublica
    # preprocessing
    data["fu_time"] = data['end'] - data['start']
    data = data.groupby('id').agg({
        'sex': 'first', 'age': 'first', 'race': 'first', 'priors_count': 'first',
        'c_charge_degree': 'first', 'c_charge_desc': 'first', 'is_recid': 'first', 'fu_time': 'sum' })
    data = data.loc[(data['is_recid'] == 1) | (data['fu_time'] > 730)]
    data = data.dropna()
    data.drop(['fu_time'], inplace=True, axis=1)

    # remove unfrequent charge_degrees
    counts = data.groupby('c_charge_degree').count()
    frequent_degrees = list(counts[counts['age'] > 10].index)
    data = data.loc[data['c_charge_degree'].apply(lambda x: x in frequent_degrees)]
    data['charge_type'] = data['c_charge_degree'].apply(lambda x: x[1])
    data.replace({'charge_type': {'F': 'Felony', 'M': 'Misdemeanor'}}, inplace=True)
    data['charge_gravity'] = data['c_charge_degree'].apply(lambda x: int(x[2]) if x[2] != 'O' else 3)
    del data['c_charge_degree']
    data['y_true'] = data['is_recid']
    del data['is_recid']

    # load dataset
    X, y = data.drop('y_true', axis=1), data['y_true']

    # column ordering
    X = X[["sex", "age", "race", "priors_count", "c_charge_desc", "charge_type", "charge_gravity"]]

    # encoding categorical dataset
    categorical_features = [x for x in data.columns if data[x].dtype == 'object']
    categorical_transformer = Pipeline(steps=[
        ('onehot', OneHotEncoder(handle_unknown='ignore'))])
    preprocessor = ColumnTransformer(
        transformers=[('cat', categorical_transformer, categorical_features)],
        remainder='passthrough'
    )

    neigh = KNeighborsClassifier()

    pipe = Pipeline(steps=[('one_hot', preprocessor), ('neigh', neigh)])

    # Parameters of pipelines can be set using ‘__’ separated parameter names:
    parameters = {'neigh__n_neighbors': range(2, 15)}

    search = GridSearchCV(pipe, parameters, n_jobs=-1, cv=5)
    search.fit(X, y)
    mod = search.best_estimator_
    dump(mod, os.path.join(BASE_DIR, "./generate_justifications/dataset/compas_knn.joblib"))
    data['y_pred'] = mod.predict(X)
    return data
