from collections import namedtuple
import operator
import numpy as np

class Rule:
    def __init__(self, predicate_list):
        """
        Initialize the rule as a list of predicates
        :param predicate_list: list of tuples [(var_index, operator, var_index), , ...]
        """
        Predicate = namedtuple("Predicate", "l_index operator r_index")
        self.predicate_list = [Predicate(*pred) for pred in predicate_list]

    def test(self, x_l, x_r, suffixes=('', '')):
        return np.array(
            [pred.operator(x_l[pred.l_index + suffixes[0]], x_r[pred.r_index + suffixes[1]])
                for pred in self.predicate_list]
        ).all(axis=0)

    def select_equal_predicates(self):
        return [pred for pred in self.predicate_list if pred.operator == operator.eq]

    def select_non_equal_predicates(self):
        return [pred for pred in self.predicate_list if pred.operator!=operator.eq]


class ContestVal:
    def __init__(self, predicate_list, target_c, values):
        self.type = 'values'
        self.target_c = target_c
        self.values = values
        self.rule = Rule(predicate_list)

    def test_antecedent(self, x):
        return self.rule.test(x, self.values)

    def test_consequent(self, x, how='both', negative=False):
        if how == 'both':
            if negative:
                test_output = (x.y_pred != self.target_c) & (x.y_true != self.target_c)
            else:
                test_output = (x.y_pred == self.target_c) & (x.y_true == self.target_c)
        elif how == 'pred':
            if negative:
                test_output = x.y_pred != self.target_c
            else:
                test_output = x.y_pred == self.target_c
        elif how == 'true':
            if negative:
                test_output = x.y_true != self.target_c
            else:
                test_output = x.y_true == self.target_c
        else:
            raise ValueError("'how' argument should be both, pred or true")
        return test_output

    def test(self, x, negative=False, how='both'):
        return self.test_antecedent(x) & self.test_consequent(x, negative=negative, how=how)


class ContestRel:
    def __init__(self, predicate_list, target_plaintiff, output_relative):
        self.type = 'relative'
        self.target_plaintiff = target_plaintiff
        self.output_relative = output_relative
        self.rule = Rule(predicate_list)

    def test_antecedent(self, x_l, x_r, how='both', suffixes=('', '')):
        feature_test = self.rule.test(x_l, x_r, suffixes=suffixes)
        if how == 'both':
            output_test = (
                    (x_r['y_pred' + suffixes[1]] == self.output_relative)
                    & (x_r['y_true' + suffixes[1]] == self.output_relative)
            )
        elif how == 'pred':
            output_test = (x_r['y_pred' + suffixes[1]] == self.output_relative)
        elif how == 'true':
            output_test = (x_r['y_true' + suffixes[1]] == self.output_relative)
        else:
            raise ValueError("'how' argument should be both, pred or true")
        return feature_test & output_test

    def test_consequent(self, x_l, x_r,  negative=False, how='both', suffixes=('', '')):
        if negative:
            if how == 'both':
                test_output = (
                            (x_l['y_pred' + suffixes[0]] != self.target_plaintiff)
                            & (x_l['y_true' + suffixes[0]] != self.target_plaintiff)
                    )
            elif how == 'pred':
                test_output = (x_l['y_pred' + suffixes[0]] != self.target_plaintiff)
            elif how == 'true':
                test_output = (x_l['y_true' + suffixes[0]] != self.target_plaintiff)
            else:
                raise ValueError("'how' argument should be both, pred or true")
            return test_output
        else:
            if how == 'both':
                test_output = (
                            (x_l['y_pred' + suffixes[0]] == self.target_plaintiff)
                            & (x_l['y_true' + suffixes[0]] == self.target_plaintiff)
                    )
            elif how == 'pred':
                test_output = (x_l['y_pred' + suffixes[0]] == self.target_plaintiff)
            elif how == 'true':
                test_output = (x_l['y_true' + suffixes[0]] == self.target_plaintiff)
            else:
                raise ValueError("'how' argument should be both, pred or true")
            return test_output

    def test(self, x_l, x_r, negative=False, how='both', suffixes=('', '')):
        return (
                self.test_antecedent(x_l, x_r, suffixes=suffixes)
                &
                self.test_consequent(x_l, x_r, negative=negative, how=how, suffixes=suffixes)
        )

